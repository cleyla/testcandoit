<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Payments Refund Confirmation</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments payments-refund-confirmation">
            <div class="row">
                <div class="col-md-3 col-xl-3">
                    <div class="left-description">
                        <h3><a href="">PAYMENT REFUND</a></h3>
                        <p>A Ethereum miner fee of 0.00000347 ETH will be charged to your ledger when mercurygate processes the refund.</p>
                        <p>Note: Once processed, Bitcoin transactions are not reversible.</p>
                        <div class="btn-general">
                            <a href="#" class="" data-toggle="modal" data-target="#exampleModal">REFUND PAYMENT</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-xl-9">
                    <div class="refund-details-confirmation">
                       <h3>refund details</h3>
                       <div class="input-inline">
                            <label>From</label>
                            <input type="text" name="" value="m.pirro1987@gmail.com">
                        </div>
                        <div class="input-inline">
                            <label>Refund Amount</label>
                            <input type="text" name="" value="3,568.12 USD 0.00000000 ETH">
                        </div>
                        <div class="description-refund">
                            <p>If you are sure you would like to refund this payment, please<br> enter the refund amount (3.568.12 USD)</p>
                            <input type="number" name="" placeholder="0.00">
                        </div>
                    </div>
                </div>
            </div>
            <?php include('modal_refund.php'); ?>
        </div>
    </div>      
    


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>