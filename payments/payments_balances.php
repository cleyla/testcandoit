<div class="payments-transactions">
    <div class="balance-ethereum">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="switchEth" checked="checked">
            <label class="custom-control-label" for="switchEth"></label>
        </div>
        <div class="title-balance">
            <p>Eth Balance</p>
        </div>
        <div class="amount-balance">
            <p>0.00012345</p>
        </div>
    </div>
    <div class="balance-bitcoin">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="switchBit" checked="checked">
            <label class="custom-control-label" for="switchBit"></label>
        </div>
        <div class="title-balance">
            <p>BTC Balance</p>
        </div>
            <div class="amount-balance">
                <p>0.00012345</p>
            </div>
    </div>
    <div class="balance-dash">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="switchDash" checked="checked">
            <label class="custom-control-label" for="switchDash"></label>
        </div>
        <div class="title-balance">
            <p>Dash Balance</p>
        </div>
        <div class="amount-balance">
            <p>0.00012345</p>
        </div>
    </div>
</div>
<div class="btn-general">
    <a href="#" class="btn-documentation">Documentation</a>
    <a href="#" class="btn-help">Help + support</a>
</div>