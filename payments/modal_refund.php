<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h5>Write the verification code</h5>
        <p>From the google authenticator app</p>
        <input type="number" placeholder="888888">
      </div>
      <div class="modal-footer">
        <div class="btn-footer">
            <button type="button" class="">Verify</button>
            <button type="button" class="" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>