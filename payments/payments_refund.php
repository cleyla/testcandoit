<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Payments Refund</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments payments-refund">
            <div class="row">
                <div class="col-md-3 col-xl-3">
                    <div class="left-description">
                        <h3><a href="">PAYMENT REFUND</a></h3>
                        <div class="refund-amount">
                            <h4>Refund amount</h4>
                            <p>Please choose how much of this payment you would like to refound. If partial, you will have to enter the amount manually.</p>
                        </div>
                        <div class="refundable-total">
                            <h4>refundable total</h4>
                            <p>$3,568.12</p>
                        </div>
                        <div class="customer-email">
                            <h4>Customer email address</h4>
                            <p>The customer will receive a special link at this email address that will allow them to claim their refund.</p>
                        </div>
                        <div class="btn-general">
                            <a href="http://localhost/mercurygatefe/payments/payments_refund_confirmation.php" class="">VIEW CONFIRMATION</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-xl-9">
                    <div class="refund-details">
                        <div class="radio-refund">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="fullRefund" name="optionRefund" value="fullRefund" onChange="radioRefund(this.value);" checked="checked">
                                <label class="custom-control-label" for="fullRefund">Full Refund</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="partialRefund" name="optionRefund" value="partialRefund" onChange="radioRefund(this.value);">
                                <label class="custom-control-label" for="partialRefund">Partial Refund</label>
                            </div>
                            <div id="addOption" class="input-amount">
                                <input type="text" name="" placeholder="Please enter a mount">
                            </div>
                        </div>
                        <div class="radio-email">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="providedEmail" name="optionEmail" value="providedEmail" onChange="radioEmail(this.value);" checked="checked">
                                <label class="custom-control-label" for="providedEmail">User provided email address (recomended)</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="specifyEmail" name="optionEmail" value="specifyEmail" onChange="radioEmail(this.value);">
                                <label class="custom-control-label" for="specifyEmail">Specify a different email address</label>
                            </div>
                            <div id="addEmail" class="input-amount">
                                <input type="email" name="" placeholder="Please enter a email address">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>