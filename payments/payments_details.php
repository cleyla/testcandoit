<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Payments Details</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments payments-details">
            <div class="row">
                <div class="col-md-12 col-xl-3  ">
                    <div class="left-description">
                        <h3><a href="">PAYMENT DETAILS</a></h3>
                        <div class="title-currency">
                            <h4>Currency</h4>
                            <div class="currency-ethereum"><p>Ethereum</p></div>
                        </div>
                        <div class="amount-currency">
                            <h4>Amount</h4>
                            <p>8.000000000 eth</p>
                        </div>
                        <div class="value-usd">
                            <h4>Value (USD)</h4>
                            <p>$3,568.12</p>
                        </div>
                        <div class="rate">
                            <h4>Rate</h4>
                            <p>"Record $442.39"</p>
                            <p>"($140 Current value)"</p>
                        </div>
                        <div class="auto-convert">
                            <h4>Auto convert</h4>
                            <p>Wallet Address <br>00/00/0000<br>00:00 PM</p>
                            <p>Rate day:<br>0.0000000</p>
                        </div>
                        <div class="btn-general">
                            <a href="http://localhost/mercurygatefe/payments/payments_refund.php" class="btn-refund">Refund</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xl-9">
                    <div class="invoice-details">
                        <h3>Invoice Details</h3>
                        <div class="group-1">
                            <div class="input-inline img-user">
                                <label>From</label>
                                <img src="../img/photo-customer-2-mercury-gate.png">
                                <input type="text" name=""  value="Amy Hunt" class="input-large">
                            </div>
                            <div class="input-inline text-status">
                                <label>Status</label>
                                <div class="status-pending"></div>
                                <input type="text" name="" placeholder="PENDINg" class="input-large">
                            </div>
                            <div class="input-inline">
                                <label>Payment Type</label>
                                <div class="icon-pas"></div>
                                <input type="text" name="" placeholder="Point of sale app" class="input-large">
                            </div>
                            <div class="input-inline">
                                <label>Paid with auto convert option</label>
                                <div class="icon-approved"></div>
                                <input type="text" name="" placeholder="From ETH to USD" class="input-large">
                            </div>
                        </div>
                        <div class="group-2">
                            <div class="input-inline">
                                <label>Order ID</label>
                                <input type="text" name=""  value="0002" class="input-1">
                            </div>
                            <div class="input-inline text-status">
                                <label>Confirmations</label>
                                <input type="text" name="" value="8" class="input-2">
                            </div>
                            <div class="input-inline">
                                <label>Date</label>
                                <input type="text" name="" value="00/00/0000 00:00 AM" class="input-3">
                            </div>
                            <div class="input-inline color-hash">
                                <label>Hash</label>
                                <input type="text" name="" value="3b8304a7a2b244f7c51ee9c242040939 " class="input-4">
                            </div>
                        </div>
                        <div class="product-comments">
                            <h5>Product / Comments</h5>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas itaque amet asperiores commodi ex ea iusto. Quae corporis consectetur, assumenda cumque accusamus omnis, sed molestias aliquam nulla cum eius aliquid?</p>
                        </div>
                        <div class="status-payment-general">
                            <h5>Status</h5>
                            <div class="status-payment">
                                <div class="status-1">
                                    <p><span>Payment received for 8.0000000 ETH</span><br>00/00/0000 00:00 AM</p>
                                </div>
                                <div class="status-2">
                                    <p><span>Invoice created</span><br>00/00/0000 00:00 AM</p>
                                </div>
                            </div>
                        </div>
                             
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>