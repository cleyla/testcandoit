<div class="table-payments-transactions">
	<div class="filter-general">
		<div class="title-table-payments">
			<h3>LATEST transactions</h3>
		</div>
		<div class="box-hide">
			<div class="auto-convert">
				<p class="title-p">Show in</p>
				<p class="sub-title-p">USD</p>
				<div class="custom-control custom-switch">
					<input type="checkbox" class="custom-control-input" id="autoConvert" checked="checked">
					<label class="custom-control-label" for="autoConvert"></label>
				</div>
			</div>
			<div class="dropdown-transactions-payments">
				<button class="dropdown-toggle btn-all-accounts" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Filter by
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<li class="dropdown-submenu">
						<a  class="dropdown-item" tabindex="-1" href="#">Payment Type</a>
						<ul class="dropdown-menu menu-custom">
							<li class="dropdown-item"><a class="opt-email-b" tabindex="-1" href="#">EMAIL BILLING</a></li>
							<li class="dropdown-item"><a class="opt-quick-c" tabindex="-1" href="#">QUICK CHECKOUT</a></li>
							<li class="dropdown-item"><a class="opt-point-s" tabindex="-1" href="#">POINT OF SALE</a></li>
							<li class="dropdown-item"><a class="opt-donation" tabindex="-1" href="#">DONATION</a></li>
							<li class="dropdown-item"><a class="opt-tips" tabindex="-1" href="#">TIPS</a></li>
						</ul>
					</li>
					<li class="dropdown-submenu">
						<a  class="dropdown-item" tabindex="-1" href="#">Status</a>
						<ul class="dropdown-menu menu-custom-status">
							<li class="dropdown-item"><a class="opt-pending" tabindex="-1" href="#">PENDING</a></li>
							<li class="dropdown-item"><a class="opt-approved" tabindex="-1" href="#">APPROVED</a></li>
							<li class="dropdown-item"><a class="opt-declined" tabindex="-1" href="#">DECLINED</a></li>
							<li class="dropdown-item"><a class="opt-refunded" tabindex="-1" href="#">Refunded</a></li>
							<li class="dropdown-item"><a class="opt-unresolved" tabindex="-1" href="#">UNRESOLVED</a></li>
						</ul>
					</li>
				</div>
			</div>
			<div class="date-in">
				<p class="title-p">DATE IN</p>
				<p class="sub-title-p">04-06-2019</p>
			</div>
			<div class="date-out">
				<p class="title-p">DATE OUT</p>
				<p class="sub-title-p">04-06-2019</p>
			</div>
		</div>
	</div>
	<div class="table-responsive table-mobile">
		<table id="dataPaymentsTransactions" class="table fixed_header">
			<thead>
				<tr>
				    <th scope="col">Date</th>
				    <th scope="col">Currency</th>
				    <th scope="col">Payment Type</th>
				    <th scope="col">Amount</th>
				    <th scope="col">Customer</th>
				    <th scope="col">Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>2018-12-27</td>
				    <td><img src="../img/logo-bitcoin-26x25-mercury-gate.png" alt="Logo Bitcoin Mercury Gate" title="Logo Bitcoin Mercury Gate"></td>
				    <td>point of sale app</td>
				    <td>0.00000000 BTC</td>
				    <td><img src="../img/photo-customer-1-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				   	<td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
				    <td>point of sale app</td>
				    <td>0.00000000 DASH</td>
				    <td><img src="../img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-pending"><a href="http://localhost/mercurygatefe/payments/payments_details.php">Pending</a></td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-ethereum-26x25-mercury-gate.png" alt="Logo Ethereum Mercury Cash" title="Logo Ethereum Mercury Gate"></td>
				    <td>email billing</td>
				    <td>0.00000000 ETH</td>
				    <td><img src="../img/photo-customer-3-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-ethereum-26x25-mercury-gate.png" alt="Logo Ethereum Mercury Cash" title="Logo Ethereum Mercury Gate"></td>
				    <td>DONATION</td>
				    <td>0.00000000 DASH</td>
				    <td class="without-photo"><span>JH</span></td>
				    <td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-bitcoin-26x25-mercury-gate.png" alt="Logo Bitcoin Mercury Cash" title="Logo Bitcoin Mercury Gate"></td>
				    <td>DONATION</td>
				    <td>0.00000000 BTC</td>
				    <td><img src="../img/photo-customer-1-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>email billing</td>
					<td>0.00000000 dash</td>
					<td><img src="../img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
					<td class="status-approved ">Aproved</td>
				</tr>	
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>tip</td>
					<td>0.00000000 dash</td>
					<td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>email billing</td>
					<td>0.00000000 dash</td>
					<td><img src="../img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
					<td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>quick checkout for web</td>
					<td>0.00000000 dash</td>
					 <td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>tip</td>
					<td>0.00000000 dash</td>
					<td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>email billing</td>
					<td>0.00000000 dash</td>
					<td><img src="../img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
					<td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>quick checkout for web</td>
					<td>0.00000000 dash</td>
					 <td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>quick checkout for web</td>
					<td>0.00000000 dash</td>
					 <td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>quick checkout for web</td>
					<td>0.00000000 dash</td>
					 <td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>							
			</tbody>
		</table>
	</div>
</div>