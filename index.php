<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('header_home.php'); ?>
    <div class="container-general">
        <div class="container-dashboard">
            <div class="row">
                <div class="col-md-3">
                    <div class="balances-dashboard">
                        <h3>cryptocurrencies</h3>
                        <div class="balance-ethereum">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="switchEth" checked="checked">
                                <label class="custom-control-label" for="switchEth"></label>
                            </div>
                            <div class="title-balance">
                                <p>Eth Balance</p>
                            </div>
                            <div class="amount-balance">
                                <p>0.00012345</p>
                            </div>
                        </div>
                        <div class="balance-bitcoin">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="switchBit" checked="checked">
                                <label class="custom-control-label" for="switchBit"></label>
                            </div>
                            <div class="title-balance">
                                <p>BTC Balance</p>
                            </div>
                            <div class="amount-balance">
                                <p>0.00012345</p>
                            </div>
                        </div>
                        <div class="balance-dash">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="switchDash" checked="checked">
                                <label class="custom-control-label" for="switchDash"></label>
                            </div>
                            <div class="title-balance">
                                <p>Dash Balance</p>
                            </div>
                            <div class="amount-balance">
                                <p>0.00012345</p>
                            </div>
                        </div>
                    </div>

                    <div class="btn-general">
                        <a href="#" class="btn-documentation">Documentation</a>
                        <a href="#" class="btn-help">Help + support</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="graph-dashboard">
                        <div class="title-graph-left">
                            <h3>DAILY REVENUE</h3>
                        </div>
                        <div class="title-graph-right">
                            <p>DATE AS OF <span>29-05-2019</span> <span class="separator-g">|</span> DATE TO OF <span>05-06-2019</span></p>
                        </div>
                        <img src="img/graph-example-mercury-gate.png">
                    </div>
                    <?php include('table_recent_transactions.php'); ?>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="js/main.js"></script>
 
  </body>

</html>