<div class="balance-transactions">
	<div class="title-table-dashboard">
		<h3>RECENT TRANSACTIONS</h3>
	</div>
	<div class="dropdown-transactions-dashboard">
		<button class="dropdown-toggle btn-all-accounts" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    Filter by
		</button>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item opt-status" href="#">All status</a>
			<a class="dropdown-item opt-approved" href="#">Approved</a>
			<a class="dropdown-item opt-pending" href="#">Pending</a>
			<a class="dropdown-item opt-declined" href="#">Declined</a>
		</div>
	</div>
	<div class="table-responsive table-mobile">
		<table id="dataRecentTransactions" class="table fixed_header">
			<thead>
				<tr>
				    <th scope="col">Date</th>
				    <th scope="col">Currency</th>
				    <th scope="col">Payment Type</th>
				    <th scope="col">Amount</th>
				    <th scope="col">Customer</th>
				    <th scope="col">Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>2018-12-27</td>
				    <td><img src="img/logo-bitcoin-26x25-mercury-gate.png" alt="Logo Bitcoin Mercury Gate" title="Logo Bitcoin Mercury Gate"></td>
				    <td>point of sale app</td>
				    <td>0.00000000 BTC</td>
				    <td><img src="img/photo-customer-1-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				   	<td>2018-12-27</td>
				    <td><img src="img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
				    <td>point of sale app</td>
				    <td>0.00000000 DASH</td>
				    <td><img src="img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-ethereum-26x25-mercury-gate.png" alt="Logo Ethereum Mercury Cash" title="Logo Ethereum Mercury Gate"></td>
				    <td>email billing</td>
				    <td>0.00000000 ETH</td>
				    <td><img src="img/photo-customer-3-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-ethereum-26x25-mercury-gate.png" alt="Logo Ethereum Mercury Cash" title="Logo Ethereum Mercury Gate"></td>
				    <td>DONATION</td>
				    <td>0.00000000 DASH</td>
				    <td class="without-photo"><span>JH</span></td>
				    <td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-bitcoin-26x25-mercury-gate.png" alt="Logo Bitcoin Mercury Cash" title="Logo Bitcoin Mercury Gate"></td>
				    <td>DONATION</td>
				    <td>0.00000000 BTC</td>
				    <td><img src="img/photo-customer-1-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
				    <td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>email billing</td>
					<td>0.00000000 dash</td>
					<td><img src="img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
					<td class="status-approved ">Aproved</td>
				</tr>	
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>tip</td>
					<td>0.00000000 dash</td>
					<td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>email billing</td>
					<td>0.00000000 dash</td>
					<td><img src="img/photo-customer-2-mercury-gate.png" alt="Photo Customer Mercury Gate"></td>
					<td class="status-approved ">Aproved</td>
				</tr>
				<tr>
				    <td>2018-12-27</td>
				    <td><img src="img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>quick checkout for web</td>
					<td>0.00000000 dash</td>
					 <td class="without-photo"><span>JH</span></td>
					<td class="status-pending">Pending</td>
				</tr>							
			</tbody>
		</table>
	</div>
</div>
