    $("#newfile").hide();
    $('input[type="radio"]').click(function () {
        var radiovalue = $(this).val();
        if (radiovalue === 'option01')
            $('#newfile').show();
        else {
            $("#newfile").hide();
        }
    });
     $('.subcontentradio').hide();
     $('input[type="radio"]').click(function () {
         var radiovalue = $(this).val();
         if (radiovalue === 'PC00' || radiovalue === 'PC00-1')
             $('.subcontentradio').show();
         else {
             $(".subcontentradio").hide();
         }
     });

$('.btn-invite').click('change', function (e) {
    $('.container-user-main').addClass('d-none');
    $('.container-invite').addClass('d-block');
});

 jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div><span>%</span></div>').insertAfter('.quantity input');
 jQuery('.quantity').each(function () {
     var spinner = jQuery(this),
         input = spinner.find('input[type="number"]'),
         btnUp = spinner.find('.quantity-up'),
         btnDown = spinner.find('.quantity-down'),
         min = input.attr('min'),
         max = input.attr('max');

     btnUp.click(function () {
         var oldValue = parseFloat(input.val());
         if (oldValue >= max) {
             var newVal = oldValue;
         } else {
             var newVal = oldValue + 1;
         }
         spinner.find("input").val(newVal);
         spinner.find("input").trigger("change");
     });

     btnDown.click(function () {
         var oldValue = parseFloat(input.val());
         if (oldValue <= min) {
             var newVal = oldValue;
         } else {
             var newVal = oldValue - 1;
         }
         spinner.find("input").val(newVal);
         spinner.find("input").trigger("change");
     });

 });

 $('.btn-settlements').click('change', function (e) {
     $('.settlements_main').addClass('d-none');
     $('.settlements_currency').addClass('d-block');
 });