/* -------------------------------------------------------------
            bootstrapTabControl
        ------------------------------------------------------------- */
        function bootstrapTabControl(){
          var i, items = $('.nav-custom'), pane = $('.tab-pane');
          // next
          $('.nexttab').on('click', function(){
              for(i = 0; i < items.length; i++){
                  if($(items[i]).hasClass('active') == true){
                      break;
                  }
              }
              if(i < items.length - 1){
                  // for tab
                  $(items[i]).removeClass('active');
                  $(items[i+1]).addClass('active');
                  // for pane
                  $(pane[i]).removeClass('show active');
                  $(pane[i+1]).addClass('show active');
              }

          });
          // Prev
          $('.prevtab').on('click', function(){
              for(i = 0; i < items.length; i++){
                  if($(items[i]).hasClass('active') == true){
                      break;
                  }
              }
              if(i != 0){
                  // for tab
                  $(items[i]).removeClass('active');
                  $(items[i-1]).addClass('active');
                  // for pane
                  $(pane[i]).removeClass('show active');
                  $(pane[i-1]).addClass('show active');
              }
          });
      }
      bootstrapTabControl();

// Datatable

$('#dataRecentTransactions').DataTable( {
    fixedHeader: true,
    "scrollY":        "420px",
    "scrollCollapse": true
} );

$('#dataPaymentsTransactions').DataTable( {
    fixedHeader: true,
    "pageLength": 10,
    "scrollY":        "625px",
    "scrollCollapse": true,
    "dom": '<"top"pf>',
    "pagingType": "simple",
      language: {
        search: "",
        searchPlaceholder: "Search",
        paginate: {
          previous: "<i class='fas fa-angle-left'></i>",
          next: "<i class='fas fa-angle-right'></i>"
        }
      }
} );

//SWITCH BALANCE DASHBOARD


$('#switchEth').on('change', function(e){
   if(e.target.checked){
    $('.balance-ethereum').removeClass('balance-gray-ethereum');
  } else{
      $('.balance-ethereum').addClass('balance-gray-ethereum');
   }
});

$('#switchBit').on('change', function(e){
   if(e.target.checked){
    $('.balance-bitcoin').removeClass('balance-gray-bitcoin');
  } else{
      $('.balance-bitcoin').addClass('balance-gray-bitcoin');
   }
});

$('#switchDash').on('change', function(e){
   if(e.target.checked){
    $('.balance-dash').removeClass('balance-gray-dash');
  } else{
      $('.balance-dash').addClass('balance-gray-dash');
   }
});



$('.btn-146x57').on('click',function(){
    $('.box-code-1').addClass('active');
    $('.btn-preview-1').addClass('active');
    $('.box-code-2').removeClass('active');
    $('.box-code-3').removeClass('active');
    $('.btn-preview-2').removeClass('active');
    $('.btn-preview-3').removeClass('active');
});

$('.btn-168x65').on('click',function(){
   $('.box-code-2').addClass('active');
   $('.btn-preview-2').addClass('active');
   $('.box-code-1').removeClass('active');
   $('.box-code-3').removeClass('active');
   $('.btn-preview-1').removeClass('active');
   $('.btn-preview-3').removeClass('active');
});

$('.btn-210x82').on('click',function(){
   $('.box-code-3').addClass('active');
   $('.btn-preview-3').addClass('active');
   $('.box-code-1').removeClass('active');
   $('.box-code-2').removeClass('active');
   $('.btn-preview-1').removeClass('active');
   $('.btn-preview-2').removeClass('active');
});

//HOSTED CATALOG COPY HTML



$('a[href="#ac-url"]').click(function(){
   $('.code-1-general').addClass('active');
   $('.code-2-general').removeClass('active');
   $('.code-3-general').removeClass('active');
   $('.code-4-general').removeClass('active');
   $('.code-5-general').removeClass('active');
 });

 $('a[href="#ac-small"]').click(function(){
   $('.code-2-general').addClass('active');
   $('.code-1-general').removeClass('active');
   $('.code-3-general').removeClass('active');
   $('.code-4-general').removeClass('active');
   $('.code-5-general').removeClass('active');

 });

 $('a[href="#ac-medium"]').click(function(){
   $('.code-3-general').addClass('active');
   $('.code-1-general').removeClass('active');
   $('.code-2-general').removeClass('active');
   $('.code-4-general').removeClass('active');
   $('.code-5-general').removeClass('active');

 });

 $('a[href="#ac-large"]').click(function(){
   $('.code-4-general').addClass('active');
   $('.code-1-general').removeClass('active');
   $('.code-2-general').removeClass('active');
   $('.code-3-general').removeClass('active');
   $('.code-5-general').removeClass('active');

 });

 $('a[href="#ac-qr"]').click(function(){
   $('.code-5-general').addClass('active');
   $('.code-1-general').removeClass('active');
   $('.code-2-general').removeClass('active');
   $('.code-3-general').removeClass('active');
   $('.code-4-general').removeClass('active');

 });




// Add Buyer Address

function addBuyer(value) {
   if ((value == "yesBuyer")){
       document.getElementById("addBuyer").style.display = "block";
   }else{
     document.getElementById("addBuyer").style.display = "none";
   }
 }


 $(document).ready(function(){
   $(".addItem").click(function(){
       $(".item-new-bill").eq(0).clone().insertAfter(".item-new-bill-add");
   });
});

// Search Payments

$('.form-control').on('click',function(){
  $('.box-hide').toggleClass('hide');
  $('.pagination').toggleClass('hide');
  $('#dataPaymentsTransactions_filter .form-control').toggleClass('input-large');
  
});

// Add Option Refund

function radioRefund(value) {
  if ((value == "partialRefund")){
      document.getElementById("addOption").style.display = "block";
  }else{
    document.getElementById("addOption").style.display = "none";
  }
}

// Add Option Email

function radioEmail(value) {
  if ((value == "specifyEmail")){
      document.getElementById("addEmail").style.display = "block";
  }else{
    document.getElementById("addEmail").style.display = "none";
  }
}

$('.tabs').click(function () { 
  $('.tabs').removeClass('active');
  $(this).addClass('active');

});

// COPIED URL

$('a[href="#url_copy"]').click(function(){
  $('.copied-to-clipboard').addClass('show');
  $('.trouble_paying').addClass('hide');
});

$('a[href="#close_msg"]').click(function(){
  $('.copied-to-clipboard').removeClass('show');
});

$('a[href="#info_ad"]').click(function(){
  $('.information-ad').addClass('show');
  // $('.code-2-general').removeClass('active');
});

$('a[href="#copy_payments"]').click(function(){
  $('.information-ad').removeClass('show');
  $('.copied-to-clipboard').addClass('show');
});

$('a[href="#got_it"]').click(function(){
  $('.awaiting-payment').addClass('hide');
});

// Sent bill

$('a[href="#sent_bill"]').click(function(){
  $('.success-resent').addClass('show');
  $('.duplicate-bill').removeClass('show');
});

$('a[href="#duplicate_bill"]').click(function(){
  $('.duplicate-bill').addClass('show');
  $('.success-resent').removeClass('show');
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip('show')
})


// Night mode

$('#turnShedule').on('change', function(e){
  if(e.target.checked){
   $('.options-shedule').addClass('show');
 } else{
     $('.options-shedule').removeClass('show');
  }
});


function turnOnShedule(value) {
  if ((value == "turnOn")){
      document.getElementById("options-custom").style.display = "block";
  }else{
    document.getElementById("options-custom").style.display = "none";
  }
}
