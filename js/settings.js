$(document).ready(function () {
    $('.js-example-basic-single').select2();
});

$('#switch').on('change', function (e) {
    if (e.target.checked) {
        $('.support-website').addClass('d-none');
        $('.support-email').addClass('d-block');
        $('.support-website').removeClass('d-block');
        $('.support-email').removeClass('d-none');
    } else {
        $('.support-website').addClass('d-block');
        $('.support-email').addClass('d-none');
        $('.support-website').removeClass('d-none');
        $('.support-email').removeClass('d-block');
    }
});
$('#switchRL').on('change', function (e) {
    if (e.target.checked) { 
        $('.contentRL').removeClass('d-block');
        $('.contentRL').addClass('d-none');
    } else {
       $('.contentRL').addClass('d-block');
        $('.contentRL').removeClass('d-none');
    }
});
$('#switchIL').on('change', function (e) {
    if (e.target.checked) { 
        $('.contentIL').removeClass('d-block');
        $('.contentIL').addClass('d-none');
    } else {
       $('.contentIL').addClass('d-block');
        $('.contentIL').removeClass('d-none');
    }
});
$('.btn-apply').click('change', function (e) {
    $('.container-approved').addClass('d-none');
    $('.container-apply').addClass('d-block');
});
 $('.btn-slim.plus').click('change', function (e) {
     $('.token-create').removeClass('d-none');
     $('.token-main').addClass('d-none');
 });