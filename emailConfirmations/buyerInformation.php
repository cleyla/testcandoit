<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Buyer Information</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body class="body-color">

    <div class="container-general">
        <div class="email-template modal-dialog modal-xl">
            <div class="content-centered">
                <div class="row">
                    <div class="col d-flex justify-content-center mb-4">
                        <img src="../img/logo-white-mercury-gate.png" alt="Logo Mercury Gate" title="Logo Mercury Gate">
                    </div>
                </div>
                <div class="center-form">
                    <div class="row">
                        <div class="col-lg-6 form-centered-left">
                            <div class="col  top-content">
                                <h3 class="mb-4">Buyer Information</h3>
                            </div>
                            <div class="row mg-table">
                                <div class="col-md-4 mg-table-th">
                                    <span>Email Address</span>
                                </div>
                                <div class="col-md-8 mg-table-td">
                                    <span>m.pirro1987@gmail.com</span>
                                </div>                                
                                <div class="col-md-4 mg-table-th">
                                    <span>From</span>
                                </div>
                                <div class="col-md-8 mg-table-td">
                                    <span>US.</span>
                                </div>
                                <div class="col-12">
                                    <span class="d-block mt-3 mt-md-5">Order has been paid in full.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 form-centered-right"> 
                            <div class="col top-content">
                                <h3>Bull Info</h3>
                                <div class="info-right">   
                                    <span class="d-block">Bill Number: 00123</span>
                                    <span class="d-block">Due Date 05/28/2019</span>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-between mt-4">
                                <span class="item-list">Item List</span>
                            </div>
                            <div class="table-responsive px-3">
                                <table class="table table-shop">
                                    <thead>
                                        <tr>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Item</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Amount</th>
                                        </tr>                                           
                                    </thead>                                         
                                    <tbody>
                                        <tr>
                                            <td class="form-group">
                                                <span>1</span>
                                            </td>
                                            <td>Book Design</td>
                                            <td>0.000575</td>
                                            <td>0.000575 <span>BTC</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="offset-4 offset-md-6 col-md-6 col-8">  
                                <div class="row info-order">
                                    <div class="col-12 info-title">
                                        <span>Order Total</span> 
                                    </div>
                                    <div class="col-6 info-tag">
                                        <span >Total:</span>
                                    </div>
                                    <div class="col-6 info-content">
                                        <span > 0.000575 BTC </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End center form -->
            </div>
        </div>
        <!-- modal-dialog modal-dialog-centered modal-xl -->
    </div> 
    <!-- container-general -->

<!-- Modal -->
<div class="modal fade mg-modal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body p-5 text-center">
          <img src="../img/icon-delete-mercury-gate.png" alt="Delete" class="mb-3">
        <h3>Confirm Delete Item</h3>
        <span class="d-block description-mod">Do you want to remove this itemfrom your shopping cart?</span>
        <div class="d-flex justify-content-between">
            <a href="" class="btn-delete">Delete</a> 
            <a href="" class="btn-keep">Keep</a> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>