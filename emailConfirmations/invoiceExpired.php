<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Refund is Processing</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <div class="container-general">
        <div class="white-email-template email-template modal-dialog">
            <div class="row">
                <div class="col d-flex justify-content-center mb-4">
                    <img src="../img/logo-color-mercury-gate.png" alt="Logo Mercury Gate" title="Logo Mercury Gate">
                </div>
            </div>
            <div class="center-form">
                <div class="content-email text-center">
                    <div class="row">
                        <div class="col">
                            <img src="../img/icon-information-mercury-gate.png" alt="info" class="float-right">                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">                
                            <p class="e-title00 mt-3 mb-3 mb-md-4">1BTC = 8694.75 USD</p>
                            <div class="row mg-table">
                                <div class="col-md-5 mg-table-th">
                                    <span>Amount Paid</span>
                                </div>
                                <div class="col-md-7 mg-table-td">
                                    <span>0.000460 BTC</span>
                                </div>                                
                                <div class="col-md-5 mg-table-th">
                                    <span>Network cost</span>
                                </div>
                                <div class="col-md-7 mg-table-td">
                                    <span>0.000241 BTC</span>
                                </div>
                                <div class="col-md-5 mg-table-th">
                                    <span>Total</span>
                                </div>
                                <div class="col-md-7 mg-table-td">
                                    <span>0.000701 BTC</span>
                                </div>
                            </div>
                            <p class="e-title00 mb-3 mb-md-4">Refound Address</p>
                            <p class="e-address m-0">19KAR9zGiUcmFPyFXyMPjBkmXsfNgYkAWm</p>
                        </div>                    
                    </div>
                </div>

            </div>
        <!-- End center form -->
        </div>
        <!-- email-template -->
    </div> 
    <!-- container-general -->


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>