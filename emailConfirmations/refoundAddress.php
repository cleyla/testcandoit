<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Refound Address</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <div class="container-general">
        <div class="white-email-template email-template modal-dialog">
            <div class="row">
                <div class="col d-flex justify-content-center mb-4">
                    <img src="../img/logo-color-mercury-gate.png" alt="Logo Mercury Gate" title="Logo Mercury Gate">
                </div>
            </div>
            <div class="center-form">
                <div class="content-email text-center">
                    <div class="row">
                        <div class="col">
                            <img src="../img/icon-information-mercury-gate.png" alt="info" class="float-right">                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">                
                            <img class="e-check" src="../img/icon-cycle-mercury-gate.png" alt="Cycle">
                            <p class="e-title mb-3 mb-md-4">Please provide a refund address</p>
                            <p class="e-message mx-3">To receive your refund of 0,00 USD we’ll need a BTC address from your wallet. Please open your wallet, copy a receiving address, and paste it below.</p>
                            <input type="text" class="e-input form-control mb-4 col-md-10 offset-md-1" placeholder="Refund Address">                          
                            <p class="e-info">CONTINUE</p>
                        </div>                    
                    </div>
                </div>

            </div>
        <!-- End center form -->
        </div>
        <!-- email-template -->
    </div> 
    <!-- container-general -->


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>