<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Hosted Catalog</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-hosted-catalog">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-description">
                        <h3>HOSTED CATALOG</h3>
                        <p>This utility can be used to quickly create Pay with mercurygate buttons for items hosted through mercurygate.</p>
                        <p>Mercurygate handles the checkout process, and collects the buyer's name and address if necessary.</p>
                        <p>When the transaction is completed, you will receive an order confirmation via email, and the customer will be directed back to your website.</p>
                        <p>The Pay with Mercurygate buttons support the following bitcoin payment methods:</p>
                        <ul>
                            <li>Click-to-Pay URI</li>
                            <li>Scan-to-Pay QR code</li>
                            <li>Copy/paste payment methods</li>
                        </ul>
                        <div class="btn-general">
                            <a href="http://localhost/mercurygateFE/hosted_catalog/hosted_catalog_add_item.php" class="add-item">+ ADD NEW ITEM</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="title-date">
                        <div class="title-table"><h3>All items</h3></div>
                    </div>
                    <div class="table-responsive table-mobile">
                        <table  class="table fixed_header">
                            <thead>
                                <tr>
                                    <th scope="col">Item</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">#Clicks</th>
                                    <th scope="col">#Orders</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Hosting 10GB<br> Description line</td>
                                    <td>160.00 USD</td>
                                    <td>1</td>
                                    <td>0</td>
                                    <td><a href="#"><div class="action-edit"></div></a><a href="#"><div class="action-remove"  data-toggle="modal" data-target="#deleteModal"></div></a></td>
                                </tr>
                                <tr>
                                    <td>Software Development Service<br>1 Hour for service</td>
                                    <td>9.96 USD</td>
                                    <td>1</td>
                                    <td>0</td>
                                    <td><a href=""><div class="action-edit"></div></a><a href=""><div class="action-remove"></div></a></td>
                                </tr>
                                <tr>
                                    <td>Create Product Name<br> Description line</td>
                                    <td>8.000 USD</td>
                                    <td>1</td>
                                    <td>0</td>
                                    <td><a href=""><div class="action-edit"></div></a><a href=""><div class="action-remove"></div></a></td>
                                </tr>			
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>      

<!-- Modal -->
<div class="modal fade mg-modal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body p-5 text-center">
          <img src="../img/icon-delete-mercury-gate.png" alt="Delete" class="mb-3">
        <h3>Confirm Delete Item</h3>
        <span class="d-block description-mod">Deleting this item will delete all history of your sales of this item. Do you really want to delete all history?</span>
        <div class="d-flex justify-content-between">
            <a href="" class="btn-delete">Delete</a> 
            <a href="" class="btn-keep">Keep</a> 
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>