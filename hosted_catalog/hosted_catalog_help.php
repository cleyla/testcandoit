<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Hosted Catalog Help</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-hosted-catalog">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-description">
                        <h3>HOSTED CATALOG</h3>
                        <p>Mercurygate will host a shopping cart to process sales and payments for your catalog.</p>
                        <p>This page explains the detailed instructions for adding your items to the BitPay database, and how to let your customers order these items using bitcoins.</p>
                    </div>
                </div>
                <div class="col-md-9 col-lg-7 col-xl-6">
                    <div class="help_description">
                        <h3 class="mt-4 mt-lg-0">HOSTED CATALOG</h3>
                            <p class="mt-3 mt-md-4">He item you create with the Catalog Item generator will store all of the embedded data on Mercurygate’s servers.</p>
                            <p>You can edit the item data at any time on our site through the <a href="" class="linkto-shop">My Catalog Items link</a></p>
                            <p>You can make a different Catalog Item for each product you want to sell. </p>
                            <p>Place the checkout button HTML code on the catalog pages of your website, or print the QR code on any poster, ad, or sign.</p>
                        <h3 class="mt-4 mt-md-5">Step 2. Buyer Orders from your Website</h3>
                            <p class="mt-3 mt-md-4">When a buyer clicks the website button or scans the QR image, they land on a 
                            mercurygate shopping cart with this item added. If they already have an open shopping 
                            cart for you, this item will be added to the cart. Shoppers can only have items from one merchant in a cart.</p> 
                            <p>Shoppers can change the quantity they want to order, or return to your website and add more items. </p> 
                            <p>If you update the price or description of an item at Mercurygate, it will update any open shopping carts that contain that item. </p>
                            <p>Before proceeding to checkout, the Shopper must enter the fields you selected to be required for the items (Name, Address, Email, etc).</p>
                        <h3 class="mt-4 mt-md-5">Step 3. Buyer Pays for the Order</h3>
                            <p class="mt-3 mt-md-4">When payment is received and confirmed (according to your <a href="" class="linkto-risk">Risk/Speed settings</a> ), Mercurygate will 
                            send an email to both the buyer and the merchant with a detailed receipt and summary of the order.</p>
                            <p>The buyer will also be presented with a receipt in their browser.</p>
                    </div>
                    <!-- End help_description -->
                </div>
                <!-- End col-md-9 col-lg-7 col-xl-6-->
            </div>
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>