<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Hosted Catalog</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-hosted-catalog hosted-add-item">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-description">
                        <h3>HOSTED CATALOG ITEM</h3>
                        <p>This form will create the HTML code or a scannable barcode that you can add to your website or printed materials.</p>
                        <p>This button will submit the item information and price. Mercurygate manages the shopping cart and collects the buyer's name and address if necessary.</p>
                        <p>When the transaction is completed, both the buyer and seller will receive an email order confirmation.</p>
                        <div class="div-help">
                            <a href="http://localhost/mercurygateFE/hosted_catalog/hosted_catalog_help.php">Help</a>
                        </div>
                    </div>
                    <div class="btn-general btn-test-url d-none d-lg-block">
                        <a href="#" class="btn-test">Test this URL</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="box-form copy-url">
                        <h3>EDIT AN ITEM</h3>
                        <div class="form-add-item">
                            <div class="group-1">
                                <div class="input-inline position-relative">
                                    <label>Merchant name</label>
                                    <a href="" class="edit-file"></a>
                                    <input type="text" name="" placeholder="backlayer Inc" class="input-medium"> 
                                </div>
                                <div class="input-inline">
                                    <label>Item Description</label>
                                    <input type="text" name="" placeholder="Hosting 10GB" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Item number/SKU</label>
                                    <input type="text" name="" placeholder="Description Line" class="input-medium">
                                </div>
                            </div>
                            <div class="group-2">
                                <div class="input-inline">
                                    <label>Item Price</label>
                                    <input type="text" name="" placeholder="9.95" class="input-small">
                                </div>
                                <div class="dropdown-inline">
                                    <button class="dropdown-toggle input-small" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        USD - US Dollar
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">USD</a>
                                        <a class="dropdown-item" href="#">Bitcoin</a>
                                        <a class="dropdown-item" href="#">Dash</a>
                                        <a class="dropdown-item" href="#">Ethereum</a>
                                    </div>
                                </div>
                                <div class="input-inline">
                                    <label>Shipping & Handling</label>
                                    <input type="text" name="" placeholder="Shipping & handling" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Taxt Rate (%)</label>
                                    <input type="text" name="" placeholder="Taxt Rate (%)" class="input-medium">
                                </div>
                            </div>
                            <div class="collect-buyer">
                                <h3>COLLECT BUYER’s information</h3>
                                <form>
                                    <div class="row">
                                        <div class="col-md-6 col-xl-4">
                                                <label class="custom-check-btn" for="buyer1"> Collect Buyer's Email Address (required)
                                                    <input type="checkbox" class="custom-control-input" id="buyer1" name="buyer1">
                                                    <span class="check-btn"></span>
                                                </label>
                                                <label class="custom-check-btn" for="buyer2"> Collect Buyer's Full Name
                                                    <input type="checkbox" class="custom-control-input" id="buyer2" name="buyer2">
                                                    <span class="check-btn"></span>
                                                </label>
                                        </div>
                                        <div class="col-md-6 col-xl-4">
                                            <label class="custom-check-btn" for="buyer3"> Collect Buyer's Address
                                                    <input type="checkbox" class="custom-control-input" id="buyer3" name="buyer3">
                                                    <span class="check-btn"></span>
                                                </label>
                                                <label class="custom-check-btn" for="buyer4"> Collect Buyer's Telephone Number
                                                    <input type="checkbox" class="custom-control-input" id="buyer4" name="buyer4">
                                                    <span class="check-btn"></span>
                                                </label>
                                            </div>
                                        <div class="col-md-6 col-xl-4">
                                            <div class="btn-save-changes">
                                                <a href="#">Save Changes</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="button-code">
                                <h3>Button code</h3>
                                <p>Choose one or more ways to display this item code</p>
                                <div class="box-url-general">
                                    <label>URL</label>
                                    <div class="box-url">
                                        <a href="#ac-url"><img src="../img/icon-url-mercury-gate.png"></a>
                                    </div>
                                </div>
                                <div class="small-btn-general">
                                    <label>Small Button</label>
                                    <a href="#ac-small"><img src="../img/button-checkout-146x57-mercury-gate.png"></a>
                                </div>
                                <div class="medium-btn-general">
                                    <label>Medium Button</label>
                                    <a href="#ac-medium"><img src="../img/button-checkout-168x65-mercury-gate.png"></a>
                                </div>
                                <div class="large-btn-general">
                                    <label>Large Button</label>
                                    <a href="#ac-large"><img src="../img/button-checkout-210x82-mercury-gate.png"></a>
                                </div>
                                <div class="qr-code">
                                    <label>QR Code</label>
                                    <div class="qr-border">
                                        <a href="#ac-qr"><img src="../img/qr-code-mercury-gate-57x57.png"></a>
                                    </div>
                                </div>

                                <div class="copy-url-html">
                                    <div class="code-1-general">
                                        <p>Use this URL for a buyer to add this item to a shopping cart:</p>
                                        <div class="code-1">
                                            https://bitpay.com/cart/add?itemId=YWxTz2eHVDgJviCCXHo4D8
                                        </div>
                                    </div>
                                    <div class="code-2-general">
                                        <p>Select all of the HTML code below, then copy and paste it into your web page.</p>
                                        <div class="code-1">
                                            <code>
                                            &lt;form action="https://bitpay.com/checkout" method="post"&gt;
                                                    &lt;input type="hidden" name="action" value="cartAdd" &gt;
                                                    &lt;input type="hidden" name="data" value="YWxTz2eHVDgJviCCXHo4D8" &gt;
                                                    &lt;input type="image" formtarget="blank" src="https://bitpay.com/cdn/en_US/bp-btn-pay-currencies.svg" name="submit" style="width: 126px;" alt="BitPay, the easy way to pay with bitcoins." >
                                            &lt;/form&gt;
                                            </code>
                                        </div>
                                        <h5>Preview</h5>
                                        <div class="preview-btn">
                                            <img src="../img/button-checkout-146x57-mercury-gate.png">
                                        </div>
                                    </div>
                                    <div class="code-3-general">
                                        <p>Select all of the HTML code below, then copy and paste it into your web page.</p>
                                        <div class="code-1">
                                            <code>
                                            &lt;form action="https://bitpay.com/checkout" method="post"&gt;
                                                    &lt;input type="hidden" name="action" value="cartAdd" &gt;
                                                    &lt;input type="hidden" name="data" value="YWxTz2eHVDgJviCCXHo4D8" &gt;
                                                    &lt;input type="image" formtarget="blank" src="https://bitpay.com/cdn/en_US/bp-btn-pay-currencies.svg" name="submit" style="width: 126px;" alt="BitPay, the easy way to pay with bitcoins." >
                                            &lt;/form&gt;
                                            </code>
                                        </div>
                                        <h5>Preview</h5>
                                        <div class="preview-btn">
                                            <img src="../img/button-checkout-168x65-mercury-gate.png">
                                        </div>
                                    </div>
                                    <div class="code-4-general">
                                        <p>Select all of the HTML code below, then copy and paste it into your web page.</p>
                                        <div class="code-1">
                                            <code>
                                            &lt;form action="https://bitpay.com/checkout" method="post"&gt;
                                                    &lt;input type="hidden" name="action" value="cartAdd" &gt;
                                                    &lt;input type="hidden" name="data" value="YWxTz2eHVDgJviCCXHo4D8" &gt;
                                                    &lt;input type="image" formtarget="blank" src="https://bitpay.com/cdn/en_US/bp-btn-pay-currencies.svg" name="submit" style="width: 126px;" alt="BitPay, the easy way to pay with bitcoins." >
                                            &lt;/form&gt;
                                            </code>
                                        </div>
                                        <h5>Preview</h5>
                                        <div class="preview-btn">
                                            <img src="../img/button-checkout-210x82-mercury-gate.png">
                                        </div>
                                    </div>
                                    <div class="code-5-general">
                                        <p>Right-click and Save As... the image below. Place it anywhere!</p>
                                        <div class="for-web">
                                            <h5>For Web</h5>
                                            <img src="../img/qr-code-mercury-gate-57x57.png">
                                        </div>
                                        <div class="for-print">
                                            <h5>For Print</h5>
                                            <img src="../img/qr-code-305x305.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="btn-general btn-test-url00 m-auto d-lg-none">
                    <a href="#" class="btn-test">Test this URL</a>
                </div>
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>