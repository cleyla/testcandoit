<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Hosted Catalog</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-hosted-catalog hosted-add-item">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-description">
                        <h3>HOSTED CATALOG ITEM</h3>
                        <p>This form will create the HTML code or a scannable barcode that you can add to your website or printed materials.</p>
                        <p>This button will submit the item information and price. Mercurygate manages the shopping cart and collects the buyer's name and address if necessary.</p>
                        <p>When the transaction is completed, both the buyer and seller will receive an email order confirmation.</p>
                        <div class="div-help">
                            <a href="http://localhost/mercurygateFE/hosted_catalog/hosted_catalog_help.php">Help</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                <div class="box-form">
                    <h3>ADD ITEM</h3>
                        <div class="form-add-item">
                            <div class="group-1">
                                <div class="input-inline">
                                    <label>Merchant name</label>
                                    <input type="text" name="" placeholder="backlayer Inc" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Item Description</label>
                                    <input type="text" name="" placeholder="Hosting 10GB" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Item number/SKU</label>
                                    <input type="text" name="" placeholder="Description Line" class="input-medium">
                                </div>
                            </div>
                            <div class="group-2">
                                <div class="input-inline">
                                    <label>Item Price</label>
                                    <input type="text" name="" placeholder="9.95" class="input-small">
                                </div>
                                <div class="dropdown-inline">
                                    <button class="dropdown-toggle input-small" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        USD - US Dollar
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">USD</a>
                                        <a class="dropdown-item" href="#">Bitcoin</a>
                                        <a class="dropdown-item" href="#">Dash</a>
                                        <a class="dropdown-item" href="#">Ethereum</a>
                                    </div>
                                </div>
                                <div class="input-inline">
                                    <label>Shipping & Handling</label>
                                    <input type="text" name="" placeholder="Shipping & handling" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Taxt Rate (%)</label>
                                    <input type="text" name="" placeholder="Taxt Rate (%)" class="input-small">
                                </div>
                            </div>
                            <div class="collect-buyer">
                                <h3>COLLECT BUYER’s information</h3>
                                <form>
                                     <div class="row">
                                        <div class="col-md-6 col-xl-4">
                                                <label class="custom-check-btn" for="buyer1"> Collect Buyer's Email Address (required)
                                                    <input type="checkbox" class="custom-control-input" id="buyer1" name="buyer1">
                                                    <span class="check-btn"></span>
                                                </label>
                                                <label class="custom-check-btn" for="buyer2"> Collect Buyer's Full Name
                                                    <input type="checkbox" class="custom-control-input" id="buyer2" name="buyer2">
                                                    <span class="check-btn"></span>
                                                </label>
                                        </div>
                                        <div class="col-md-6 col-xl-4">
                                            <label class="custom-check-btn" for="buyer3"> Collect Buyer's Address
                                                    <input type="checkbox" class="custom-control-input" id="buyer3" name="buyer3">
                                                    <span class="check-btn"></span>
                                                </label>
                                                <label class="custom-check-btn" for="buyer4"> Collect Buyer's Telephone Number
                                                    <input type="checkbox" class="custom-control-input" id="buyer4" name="buyer4">
                                                    <span class="check-btn"></span>
                                                </label>
                                            </div>
                                        <div class="col-md-6 col-xl-4">
                                            <div class="btn-save-changes">
                                                <a href="#">Save Changes</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>