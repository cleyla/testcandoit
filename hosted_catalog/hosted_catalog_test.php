<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Mercury Gate - Hosted Catalog Test URL</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>
  <body class="body-color">

    <div class="container-general">
        <div class="hosted-test modal-dialog modal-dialog-centered modal-xl">
            <div class="content-centered">
                <div class="row">
                    <div class="col d-flex justify-content-center mb-4">
                        <img src="../img/logo-white-mercury-gate.png" alt="Logo Mercury Gate" title="Logo Mercury Gate">
                    </div>
                </div>
                <div class="col center-form">
                    <div class="row">
                        <div class="col-lg-6 form-centered-left">
                            <div class="top-content">
                                <h3>Buyer Information</h3>
                            </div>
                            <form>
                                <div class="form-group row mt-3 mt-md-4">
                                    <label class="col-md-4 col-form-label">Email Address</label>
                                    <div class="col-md-8">
                                        <input type="email" class="form-control" placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Name</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Shipping Address</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="First Address Line #123">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-md-4 col-md-8">
                                        <input type="text" class="form-control" placeholder="Second Address Line #123">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Contry</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="Country">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">State/Province</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="State/Province">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">City</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="City">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">ZIP/Postal Code</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="ZIP/Postal Code">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Phone Number</label>
                                    <div class="col-md-8">
                                        <input type="tel" class="form-control" placeholder="Phone Number">
                                    </div>
                                </div>
                                <div class="offset-md-4 col-md-8 footer-info">
                                    By providing your information, you give explicit consent to mercurygate to store and provide this information to the merchant. <a href="" class="linkto"> View Privacy Policy.</a>
                                </div>
                                <div class="offset-md-3 col-md-8 footer-info btn-mg">
                                    <a href="#" class="btn-save">Save Changes</a>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-6 form-centered-right">
                            <div class="col top-content">
                                <h3 class="title-shop">Shop</h3>
                                <div class="tabs-crypto">
                                    <a href="#" class="tabs tab-USD"></a>
                                    <a href="#" class="tabs tab-BTC"></a>
                                    <a href="#" class="tabs tab-eth"></a>
                                    <a href="#" class="tabs tab-dash"></a>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-between mt-4">
                                <span class="item-list">Item List</span>
                                <a href="" Class="btn-rec">Recalculate</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-shop">
                                    <thead>
                                        <tr>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Item</th>
                                            <th scope="col"></th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="form-group">
                                                <input type="text" placeholder="1">
                                            </td>
                                            <td>Software Development Service 1 Hour Second Line Description DEV1HR</td>
                                            <td>
                                                <a href="#" class="icon-trash" data-toggle="modal" data-target="#deleteModal"></a>
                                            </td>
                                            <td>160.00</td>
                                            <td>160.00 USD</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                             <div class="row items-box">
                                <div class="col-8 content-items-left">
                                    <span class="title-item">Auto convert</span>
                                    <div class="row">
                                        <div class="col">
                                            <span>Wallet Address</span>
                                            <span>00/00/00 - 00:00</span>
                                            <span>Rate day:</span>
                                            <span>0.0000000</span>
                                        </div>
                                    </div>
                                    <!-- End row -->
                                </div>
                                <div class="col-4 content-items-right">
                                    <span class="title-item">Order Total</span>
                                        <div class="row ">
                                            <div class="col-4 col-xl-4 form-ch">
                                                <span>Tax</span>
                                            </div>
                                            <div class="col-8 col-xl-8 form-cb">
                                                <span>160.00</span>
                                            </div>
                                        </div>
                                        <!-- End row -->

                                        <div class="row">
                                            <div class="col-4 col-xl-4 form-ch">
                                                <span>Shipping</span>
                                            </div>
                                            <div class="col-8 col-xl-8 form-cb">
                                                <span>0.00 USD</span>
                                            </div>
                                        </div>
                                        <!-- End row -->

                                        <div class="row">
                                            <div class="col-4 col-xl-4 form-ch">
                                                <span>TOTAL</span>
                                            </div>
                                            <div class="col-8 col-xl-8 form-cb">
                                                <span>171.20</span>
                                            </div>
                                        </div>
                                        <!-- End row -->
                                </div>
                            </div>
                            <div class="row justify-content-center mt-4 mt-md-5 ">
                                <img src="../img/button-checkout-83x32-mercury-gate.png" alt="Button Checkout">
                                <span class="footer-note mt-3 mb-3">NOTE: Item Prices are subject to change until you Checkout.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End center form -->
            </div>
        </div>
        <!-- modal-dialog modal-dialog-centered modal-xl -->
    </div>
    <!-- container-general -->

<!-- Modal -->
<div class="modal fade mg-modal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body p-5 text-center">
          <img src="../img/icon-delete-mercury-gate.png" alt="Delete" class="mb-3">
        <h3>Confirm Delete Item</h3>
        <span class="d-block description-mod">Do you want to remove this itemfrom your shopping cart?</span>
        <div class="d-flex justify-content-between">
            <a href="" class="btn-delete">Delete</a>
            <a href="" class="btn-keep">Keep</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>

  </body>

</html>