<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Point of sale app</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools point-sale-app">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="left-description">
                        <h3><a href="#">Point of sale app</a></h3>
                        <p>Mercurygate Checkout</p>
                        <p>Simple & fast interface for retail and mobile businesses to accept Bitcoin and Bitcoin Cash payments.</p>
                        <p>To compliment an existing point of sale system, we recommend merchants enter orders in the existing system (either in a custom "bitcoin" payment method or a "house account"), and then collect the payment separately with mercurygate Checkout.</p>
                        <p>Visit <a href="" target="_blank">https://www.mercury.cash</a> for more information on integrating mercurygate Checkout into your business.</p>
                        <div class="btn-general">
                            <a href="#" class="btn-documentation">Documentation</a>
                            <a href="#" class="btn-help">Help + support</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="btn-payments-tools">
                        <h3>Pairing codes</h3>
                        <a href="#">
                            <div class="box-btn-pap btn-1">
                                <h5>+ ADD NEW PAIRING CODE</h5>
                                <p>Pairing codes will expire in 24 hours. Add new, change and revoke tokens at the API Tokens page.</p>
                            </div>
                        </a>
                    </div>
                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                    <div class="paring-codes">
                        <div class="box-paring-codes">
                            e6MKSGTV
                        </div>
                        <div class="box-paring-codes">
                            e6MKSGTV
                        </div>
                        <div class="box-paring-codes">
                            e6MKSGTV
                        </div>
                        <div class="box-paring-codes">
                            e6MKSGTV
                        </div>
                        <div class="box-paring-codes">
                            e6MKSGTV
                        </div>
                    </div>
                    <?php } ?>
                    <div class="install-app">
                        <h3>INSTALL THE APP</h3>
                        <a href="#"><div class="download-apple"></div></a>
                        <a href="#"><div class="download-google"></div></a>                      
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>