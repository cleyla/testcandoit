<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Payments Buttons</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <div class="container-general">
        <div class="container-payments-tools choose-currency">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo-mercury-gate">
                        <img src="../img/logo-color-mercury-gate.png" alt="Logo Mercury Gate" title="Logo Mercury Gate">
                    </div>
                    <div class="copied-to-clipboard">
                        <img src="../img/icon-clipboard-mercury-gate.png">
                        <h4>URL COPIED<br> tO CLIPBOARD</h4>
                        <p class="close-msg"><a href="#close_msg">Close</a></p>
                    </div>
                    <div class="information-ad">
                        <h5>1BTC = 8694.75 USD</h5>
                        <div class="paid-details">
                            <div class="title-paid">amount Paid</div>
                            <div class="amount-paid">0.000460 BTC</div>
                        </div>
                        <div class="paid-details">
                            <div class="icon-help"><img src="../img/icon-help-mercury-gate.png"></div>
                            <div class="title-paid">Network cost</div>
                            <div class="amount-paid">0.000241 BTC</div>
                        </div>
                        <div class="paid-details">
                            <div class="title-paid">Total</div>
                            <div class="amount-paid">0.000701 BTC</div>
                        </div>
                        <div class="copy-payments">
                            <a href="#copy_payments">Copy payment URL</a>
                        </div>
                    </div>
                    <div class="awaiting-payment">
                        <h5>Awaiting Payment</h5>
                        <p>This invoice will expire soon. Please send your payment within the remaining minutes to complete your payment.</p>
                        <a href="#got_it">Got it</a>
                    </div>
                    <div class="trouble_paying">
                        <h5>Trouble Paying?</h5>
                        <p>Mercurygate invoices can only be paid with a compatible wallet. Tap HELP ME if you are having issues.</p>
                        <ul>
                            <li><a href="">HELP ME</a></li>
                            <li><a href="#url_copy">copy payment URL</a></li>
                            <li><a href="#close_msg">Close</a></li>
                        </ul>
                    </div>
                    <div class="box-general box-pay-code">
                        <a href="#url_copy"><div class="url-copy"></div></a>
                        <h4>0.000226 BTC</h4>
                        <a href="#info_ad"><div class="icon-information"></div></a>
                        <div class="box-qr">
                            <img src="../img/qr-code-305x305.png">
                        </div>
                        <p class="p-small">send your payment within 00:00</p>
                    </div>
                    <div class="need-help">Need help?</div>
                    <a href="http://localhost/mercurygatefe/payments_tools/refund.php" class="pay-wallet">Pay In wallet</a>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>