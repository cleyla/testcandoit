<!-- Modal -->
<div class="modal fade" id="deleteBill" tabindex="-1" role="dialog" aria-labelledby="deleteBillLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <img src="../img/icon-delete-mercury-gate-20x20.png">
        <h5>Confirm Delete Bill</h5>
        <p>Please type the buyer's email address "m.pirro1987@gmail.com" to confirm that you want to delete their bill.</p>
        <h6>Buyer's email address</h6>
        <input type="email" placeholder="write the email address">
      </div>
      <div class="modal-footer">
        <div class="btn-footer">
            <button type="button" class="">Delete bill</button>
        </div>
      </div>
    </div>
  </div>
</div>