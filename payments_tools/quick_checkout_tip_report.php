<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Tip Report</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools tip-report">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-date">
                        <div class="title-table">tip report</div>
                        <div class="date-table">04-06-2019 - 04-06-2019</div>
                    </div>
                    <div class="table-responsive table-mobile">
                        <table class="table fixed_header">
                            <thead>
                                <tr>
                                    <th scope="col">My tip report</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Order</th>
                                    <th scope="col">Sales</th>
                                    <th scope="col">Tip</th>
                                    <th scope="col">Invoice</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>From Marco</td>
                                    <td>04/28</td>
                                    <td>00:00 AM</td>
                                    <td>002</td>
                                    <td>$0.00</td>
                                    <td>$0.00</td>
                                    <td class="invoice"><a href="view_success_tip_report.php">H8gA29rXp4WQ2vS9FaZ1Ym</a></td>
                                </tr>
                                <tr>
                                    <td>From Marco</td>
                                    <td>04/28</td>
                                    <td>00:00 AM</td>
                                    <td>iPhone XR-Order 2</td>
                                    <td>$0.00</td>
                                    <td>$0.00</td>
                                    <td class="invoice"><a href="view_success_tip_report.php">H8gA29rXp4WQ2vS9FaZ1Ym</a></td>
                                </tr>
                                <tr>
                                    <td>From Marco</td>
                                    <td>04/28</td>
                                    <td>00:00 AM</td>
                                    <td>Design Services</td>
                                    <td>$0.00</td>
                                    <td>$0.00</td>
                                    <td class="invoice"><a href="view_success_tip_report.php">H8gA29rXp4WQ2vS9FaZ1Ym</a></td>
                                </tr>				
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>