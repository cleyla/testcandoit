<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools sent-email-billing aproved-bill">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-description">
                        <h3><a href="#">BILL DETAILS</a></h3>
                        <div class="title-currency">
                            <h4>Currency</h4>
                            <div class="currency-dash"><p>Dash</p></div>
                        </div>
                        <div class="order-total">
                          <h4>Order Total</h4>
                          <p>0.00000000 Dash</p>
                        </div>
                        <div class="total-value">
                            <h4>Total Value</h4>
                            <p>$3,568.12</p>
                            <div class="paid-eb">
                                <p>Paid</p>
                            </div>
                        </div>
                        <div class="btn-general">
                            <a href="#" class="btn-duplicate-bill">Duplicate</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                  <div class="buyer-information">
                    <h3>Complete</h3>
                    <div class="input-inline">
                      <label>Email Address</label>
                      <input class="input-large" type="email" placeholder="replaceinfo@mail.com">
                    </div>
                    <div class="bill-info">
                      <h3>Bill info</h3>
                      <div class="input-inline">
                        <label>Currency</label>
                        <input class="input-medium" type="text" placeholder="USD">
                      </div>
                      <div class="input-inline">
                        <label>Due Date</label>
                        <input class="input-medium" type="text" placeholder="00/00/0000">
                      </div>
                      <div class="input-inline">
                        <label>Bill Number</label>
                        <input class="input-medium" type="number" placeholder="123456789">
                      </div>
                    </div>
                    <div class="item-new-bill">
                      <div class="input-inline">
                        <label>Item</label>
                        <input class="input-medium" type="text" placeholder="Write item, services or product here">
                      </div>
                      <div class="input-inline">
                        <label>Quantitly</label>
                        <input class="input-small" type="number" placeholder="0">
                      </div>
                      <div class="input-inline">
                        <label>Price USD</label>
                        <input class="input-small" type="number" placeholder="0.00">
                      </div>
                      <div class="input-inline">
                        <label>AMOUNT</label>
                        <input class="input-medium" type="number" placeholder="0.00000000 DASH">
                      </div>
                    </div>                    
                  </div>                 
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>