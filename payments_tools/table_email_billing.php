<div class="table-email-billing">
	<div class="filter-general">
		<div class="title-table-payments">
			<h3>LATEST transactions</h3>
		</div>
		<div class="auto-convert">
			<p class="title-p">AUTO CONVERT</p>
			<p class="sub-title-p">USD</p>
			<div class="convert-currency">$</div>
			<div class="custom-control custom-switch">
	            <input type="checkbox" class="custom-control-input" id="autoConvert" checked="checked">
	            <label class="custom-control-label" for="autoConvert"></label>
	        </div>
		</div>
		<div class="dropdown-transactions-payments">
			<button class="dropdown-toggle btn-all-accounts" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    Filter by
			</button>
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item opt-sent" href="#">Sent</a>
				<a class="dropdown-item opt-draft" href="#">Draft</a>
				<a class="dropdown-item opt-approved" href="#">Approved</a>
				<a class="dropdown-item opt-pending" href="#">Pending</a>
			</div>
		</div>
		<div class="date-in">
			<p class="title-p">DATE IN</p>
			<p class="sub-title-p">04-06-2019</p>
		</div>
		<div class="date-out">
			<p class="title-p">DATE OUT</p>
			<p class="sub-title-p">04-06-2019</p>
		</div>
		<div class="pagination-table">
			<p class="title-p">vieW PAGE</p>
		</div>
	</div>
	<div class="table-responsive table-mobile">
		<table id="dataPaymentsTransactions" class="table fixed_header">
			<thead>
				<tr>
				    <th scope="col">Customer</th>
				    <th scope="col">Concept</th>
				    <th scope="col">Status</th>
				    <th scope="col">Date</th>
				    <th scope="col">Currency</th>
				    <th scope="col">Amount</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
				    <td class="opt-draft"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_draft.php">Draft</a></td>
				    <td>04-06-2019</td>
				    <td><img src="../img/logo-bitcoin-26x25-mercury-gate.png" alt="Logo Bitcoin Mercury Gate" title="Logo Bitcoin Mercury Gate"></td>
				    <td>0.00000000 ETH </td>
				</tr>
				<tr>
				   	<td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
				    <td class="opt-approved"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_approved.php">Approved</a></td>
				    <td>04-06-2019</td>
				    <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
				    <td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
				    <td class="opt-sent"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_sent.php">Sent</a></td>
				    <td>04-06-2019</td>
				    <td><img src="../img/logo-ethereum-26x25-mercury-gate.png" alt="Logo Ethereum Mercury Cash" title="Logo Ethereum Mercury Gate"></td>
				    <td>0.00000000 ETH </td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
				    <td class="opt-draft"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_draft.php">Draft</a></td>
				    <td>04-06-2019</td>
				    <td><img src="../img/logo-bitcoin-26x25-mercury-gate.png" alt="Logo Bitcoin Mercury Gate" title="Logo Bitcoin Mercury Gate"></td>
				    <td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-sent"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_sent.php">Sent</a></td>
					<td>04-06-2019</td>
					<td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 ETH </td>
				</tr>	
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-approved"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_approved.php">Approved</a></td>
					<td>04-06-2019</td>
					<td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-draft"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_draft.php">Draft</a></td>
					<td>04-06-2019</td>
					<td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 ETH </td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-sent"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_sent.php">Sent</a></td>
					<td>04-06-2019</td>
					<td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-draft"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_draft.php">Draft</a></td>
					<td>04-06-2019</td>
					<td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-approved"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_approved.php">Approved</a></td>
					<td>04-06-2019</td>
					<td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 ETH </td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-draft"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_draft.php">Draft</a></td>
					<td>04-06-2019</td>
					 <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-sent"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_sent.php">Sent</a></td>
					<td>04-06-2019</td>
					 <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 BTC</td>
				</tr>
				<tr>
				    <td>Name</td>
				    <td>replate this text, Product Line description Detaills line to write title</td>
					<td class="opt-approved"><a href="http://localhost/mercurygatefe/payments_tools/bill_details_approved.php">Approved</a></td>
					<td>04-06-2019</td>
					 <td><img src="../img/logo-dash-26x25-mercury-gate.png" alt="Logo Dash Mercury Cash" title="Logo Dash Mercury Gate"></td>
					<td>0.00000000 BTC</td>
				</tr>							
			</tbody>
		</table>
	</div>
</div>