<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Merchant Profile</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools container-manage-api">              
            <div class="row mt-2 mt-sm-2">
                <div class="col-md-4 col-lg-3">
                    <h3>Manage Api Tokens</h3>
                    <div class="left-content mt-3 mt-md-5">
                        <span class="description">
                            Add new, revoke, and change <a href="" class="link-mg">mercurygate api</a>  access tokens for your mercurygate account.
                        </span>
                    </div>
                    <div class="left-content  mt-5 mt-sm-5">
                      <p class="title-cl">Pairing Codes</p>
                        <span class="description">Pairing codes expire in 24-hours and can be used once. You can either receive 
                        a pairing code from a mercurygate API Client and approve it, or generate a pairing code from this page by clicking Add 
                        New Token and then enter it into a mercurygate API Client (Example: Point-of-Sale App).
                        </span>
                    </div>
                    <div class="left-content mt-5 mt-sm-5">
                      <p class="title-cl">Capabilities</p>
                        <span class="description">A facade defines a set of limited capabilities. The "Point-of-Sale" facade has 
                            the capability to create and get invoices, while the "Merchant" facade has the capability to do most of the 
                            things you see in the Dashboard. A token provides access to the capabilities of a facade on a specific 
                            resource (a merchant organization in this case).</span>
                        </span>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 mt-5 mt-md-0">
                    <h3>Approve Token</h3>

                    <!-- Start token-main -->
                    <div class="token-main ">
                        <div class="row form-right  mt-3 mt-md-5">
                            <div class="col-12">
                                <p class="light-title ml-0 ml-md-3">To approve an API client, enter a client pairing code to view and approve access.</p>
                            </div>
                        
                            <div class="col-md-6 col-lg-5  my-2 my-md-2">
                                <input type="text" class="form-control" placeholder="Pairing Code">
                            </div>
                            <div class="btn-mg m-auto m-md-2 find">
                                <a href="" class="btn-find">Find</a>
                            </div>
                        </div>
                        <div class="row mt-5 mt-md-5  justify-content-lg-start justify-content-center text-center">
                            <div class="col-lg-2 align-self-center">
                                <span class="ml-3 mb-3 mb-lg-0 d-block">Tokens</span>
                            </div>
                            <div class="btn-mg mt-3 mt-md-0 mx-3 ml-lg-5">
                                <a href="#" class="btn-slim plus">Add New Token</a>
                            </div>
                            <div class="btn-mg mt-3 mt-md-0 mx-3 ml-lg-5">
                                <a href="" class="btn-slim help">API Help</a>
                            </div>
                        </div>
                    </div>
                    <!-- End token-main -->

                    <!-- Start token-create -->
                    <div class="token-create d-none">
                        <div class="card-add-new mt-5">
                             <!-- start card line -->
                            <div class="row card-line">
                                <div class="col-md-2 card-new-head">
                                    <span>Token Label</span>
                                </div>
                                <div class="col-md-7 col-lg-4 card-new-body">
                                    <input type="text" class="w-100" placeholder="Label">
                                </div>
                            </div> 
                             <!-- End card line -->
                             
                            <!-- Start card line -->
                            <div class="row card-line">
                                <div class="col-md-2 card-new-head">
                                    <span>Facade</span>
                                </div>
                                <div class="col-md-6 col-lg-4 card-new-body">
                                    <span>Point-of-Sale</span>
                                </div>
                            </div> 
                            <!-- End card line -->

                            <!-- Start card line -->
                            <div class="row card-line">
                                <div class="col-md-2 card-new-head">
                                    <span>Client</span>
                                </div>
                                <div class="col-md-6 col-lg-4 card-new-body">
                                    <label class="container-radio mb-0 mt-0">Required Authentication
                                        <input type="radio" name="radio" value="option02">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div> 
                            <!-- End card line -->

                            <!-- Start card line -->
                            <div class="row card-line">
                                <div class="offset-0 offset-md-2 col-md-6 col-lg-4">
                                    <span class="d-block">If authentication is enabled an API client will need to cryptographically 
                                        sign every request. Please <a href="" class="link-mg">see documentation </a> for more information.
                                    </span>
                                </div>
                            </div> 
                            <!-- End card line -->

                            <!-- Start card line -->
                            <div class="row card-line mt-3 mt-md-4">
                                <div class="col-md-2 card-new-head">
                                    <span>Information</span>
                                </div>
                                <div class="col-md-4 col-lg-3 card-new-head text-left">
                                    <span>Created <span class="font-bold">NOW</span></span>
                                </div>
                                <div class="btn-mg mt-3 mt-md-0">
                                    <a href="" class="btn-token">Add Token</a>
                                </div>
                            </div> 
                            <!-- End card line -->
                        </div>
                    </div>
                    <!-- End token-create -->

                    <!-- cards -->
                    <div class="row">
                        <!-- Start token-card -->
                        <div class="col-lg-6">
                            <div class="card card-mg-tools mt-4">
                                <div class="card-mg-tools-body">
                                    <!-- start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Token Label</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Client</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Label: iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row mg-card-tools-id">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>ID</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>TfLRtzTRBR26vp2pZ9Ji7ewQjCY48DkzUCZ</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Facade</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Point-of-Sale</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Information</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Created 5/28/2019</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->
                                </div>
                                <!-- Start card line -->
                                <div class="mg-btn-group">
                                    <div class="row no-gutters">
                                        <div class="col-4 mg-btn-group-edit active">
                                            <a href="" class="btn-edit">Edit</a>
                                        </div>
                                        <div class="col-4 mg-btn-group-save disabled">
                                            <a href="" class="btn-save">Save</a>  
                                        </div>
                                        <div class="col-4 mg-btn-group-remove disabled">
                                            <a href="" class="btn-remove">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                                <!-- End card line -->
                            </div>
                        </div>
                        <!-- End token-card -->

                        <!-- Edit token-card -->
                        <div class="col-lg-6">
                            <div class="card card-mg-tools mt-4">
                                <div class="card-mg-tools-body">
                                    <!-- start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Token Label</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <input type="text" class="w-100" placeholder="Label">
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Client</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Label: iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row mg-card-tools-id">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>ID</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>TfLRtzTRBR26vp2pZ9Ji7ewQjCY48DkzUCZ</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Facade</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Point-of-Sale</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Information</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Created 5/28/2019</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->
                                </div>
                                <!-- Start card line -->
                                <div class="mg-btn-group">
                                    <div class="row no-gutters">
                                        <div class="col mg-btn-group-edit">
                                            <a href="" class="btn-edit">Edit</a>
                                        </div>
                                        <div class="col mg-btn-group-save">
                                            <a href="" class="btn-save">Save</a>  
                                        </div>
                                        <div class="col mg-btn-group-remove">
                                            <a href="" class="btn-remove">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                                <!-- End card line -->
                            </div>
                        </div>
                        <!-- Edit token-card -->
                        
                        <!-- Start token-card -->
                        <div class="col-lg-6">
                            <div class="card card-mg-tools mt-4">
                                <div class="card-mg-tools-body">
                                    <!-- start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Token Label</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Client</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Label: iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row mg-card-tools-id">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>ID</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>TfLRtzTRBR26vp2pZ9Ji7ewQjCY48DkzUCZ</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Facade</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Point-of-Sale</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Information</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Created 5/28/2019</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->
                                </div>
                                <!-- Start card line -->
                                <div class="mg-btn-group">
                                    <div class="row no-gutters">
                                        <div class="col-4 mg-btn-group-edit active">
                                            <a href="" class="btn-edit">Edit</a>
                                        </div>
                                        <div class="col-4 mg-btn-group-save disabled">
                                            <a href="" class="btn-save">Save</a>  
                                        </div>
                                        <div class="col-4 mg-btn-group-remove disabled">
                                            <a href="" class="btn-remove">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                                <!-- End card line -->
                            </div>
                        </div>
                        <!-- End token-card -->
                        
                        <!-- Start token-card -->
                        <div class="col-lg-6">
                            <div class="card card-mg-tools mt-4">
                                <div class="card-mg-tools-body">
                                    <!-- start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4  card-mg-ch">
                                            <span>Token Label</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Client</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Label: iPhone XR</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row mg-card-tools-id">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>ID</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>TfLRtzTRBR26vp2pZ9Ji7ewQjCY48DkzUCZ</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Facade</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Point-of-Sale</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->

                                    <!-- Start card line -->
                                    <div class="row">
                                        <div class="col-4 col-xl-4 card-mg-ch">
                                            <span>Information</span>
                                        </div>
                                        <div class="col-8 col-xl-8 card-mg-cb">
                                            <span>Created 5/28/2019</span>
                                        </div>
                                    </div>
                                    <!-- End card line -->
                                </div>
                                <!-- Start card line -->
                                <div class="mg-btn-group">
                                    <div class="row no-gutters">
                                        <div class="col-4 mg-btn-group-edit active">
                                            <a href="" class="btn-edit">Edit</a>
                                        </div>
                                        <div class="col-4 mg-btn-group-save disabled">
                                            <a href="" class="btn-save">Save</a>  
                                        </div>
                                        <div class="col-4 mg-btn-group-remove disabled">
                                            <a href="" class="btn-remove">Remove</a>
                                        </div>
                                    </div>
                                </div> 
                                <!-- End card line -->
                            </div>
                        </div>
                        <!-- End token-card -->
                    </div>
                    <!-- cards -->
                    <div class="row">
                        <div class="col mg-tools-pagination">
                            <a href="" class="btn-previous">Previous Page</a>
                            <a href="" class="btn-next"> Next Page</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/settings.js"></script>

 
  </body>

</html>