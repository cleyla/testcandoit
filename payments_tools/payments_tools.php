<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Payments Tools</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools">
            <div class="row">
                <div class="col-12 col-md-12 col-xl-3">
                    <div class="btn-general">
                        <a href="#" class="btn-documentation">Documentation</a>
                        <a href="#" class="btn-help">Help + support</a>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-xl-9">
                    <div class="btn-payments-tools">
                        <h3>PAYMENT TOOLS</h3>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xl-4">
                                <a href="http://localhost/mercurygatefe/payments_tools/point_of_sale_app.php">
                                    <div class="box-btn-pt btn-1">
                                        <h5>POINT OF SALE APP</h5>
                                        <p>Mercurygate Checkout is a fast, simple interface for retail and mobile businesses to accept Bitcoin and Bitcoin Cash payments.</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-4">
                                <a href="http://localhost/mercurygatefe/payments_tools/payments_buttons.php">
                                    <div class="box-btn-pt btn-2">
                                        <h5>PAYMENT  BUTTONS</h5>
                                        <p>Add Pay with mercurygate buttons to your website to allow your customers to make payments using Bitcoin.</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xl-4">
                                <a href="http://localhost/mercurygatefe/payments_tools/quick_checkout.php">
                                    <div class="box-btn-pt btn-3">
                                        <h5>QUICK CHECKOUT FOR WEB</h5>
                                        <p>Quickly generate an invoice through a web interface.</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-4">
                                <div class="box-btn-pt btn-4">
                                    <h5>Product Catalog</h5>
                                    <p>Set up a product catalog and create buttons for customers to update their cart. Mercury gate handles the checkout process. </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xl-4">
                            <a href="http://localhost/mercurygatefe/email_billing/email_billing.php">
                                <div class="box-btn-pt btn-5">
                                    <h5>EMAIL BILLING</h5>
                                    <p>simple invoicing and billing to allow your clients to pay you using mercurygate or Bitcoin, ethereum and dash Cash.</p>
                                </div>
                            </a>
                            </div>
                        </div>
                        <div class="btn-payments-tools btn-integration">
                            <h3>Integration resources</h3>
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xl-4">
                                    <div class="box-btn-pt btn-6">
                                        <h5>MANAGE API TOKENS</h5>
                                        <p>add, remove or modify mercurygate API access tokens.</p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xl-4">
                                    <div class="box-btn-pt btn-7">
                                        <h5>LEGACY API KEYS </h5>
                                        <p>Add OR REMOVE KEYS FOR THE LEGACY V1 api. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>