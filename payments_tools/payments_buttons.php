<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Payments Buttons</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools payments-buttons">
            <div class="row">
                <div class="col-md-12 col-xl-3">
                    <div class="left-description">
                        <h3><a href="#">PAYMENT BUTTONS</a></h3>
                        <p>This button is used to complete a sale on your website.</p>
                        <p>The merchant manages the shopping cart and collects the buyers' names and addresses if necessary.</p>
                    </div>
                </div>
                <div class="col-md-12 col-xl-9">
                    <div class="create-btn-checkout">
                        <h3>Create a checkout button</h3>
                        <div class="form-btn-checkout">
                            <div class="input-inline">
                                <label>Default Price</label>
                                <input type="text" name="" placeholder="Please enter a price" class="input-medium">
                            </div>
                            <div class="dropdown-inline">
                                <button class="dropdown-toggle input-small" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    USD
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">USD</a>
                                    <a class="dropdown-item" href="#">Bitcoin</a>
                                    <a class="dropdown-item" href="#">Dash</a>
                                    <a class="dropdown-item" href="#">Ethereum</a>
                                </div>
                            </div>
                            <div class="input-inline">
                                <label>Checkout Description</label>
                                <input type="text" name="" placeholder="Please enter a price" class="input-large">
                            </div>
                            <div class="button-size">
                                <label>Button size</label>
                                <button class="btn-146x57 btn-general">146x57</button>
                                <button class="btn-168x65 btn-general">168x65</button>
                                <button class="btn-210x82 btn-general">210x82</button>
                            </div>
                            <div class="payment-notification">
                                <h3>PAYMENT NOTIFICATIONS</h3>
                                <div class="input-inline">
                                    <label>Server IPN</label>
                                    <input type="text" name="" placeholder="(optional)" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Browser Redirect</label>
                                    <input type="text" name="" placeholder="(optional)" class="input-medium">
                                </div>
                                <div class="input-inline">
                                    <label>Send Email Notifications To</label>
                                    <input type="text" name="" placeholder="(optional)" class="input-medium">
                                </div>
                            </div>                          
                        </div> 
                        <div class="generate-code">
                            <div class="row">
                                <div class="col-md-12 col-xl-8">
                                    <h3>GENERATED CODE</h3>
                                    <p>Select all of the HTML code below, then copy and paste it into your web page.</p>
                                    <div class="box-generate-code">
                                        <div class="box-code-1">
                                            <code>
                                                &lt;a href="#"&gt;
                                                &lt;div 
                                                    style="background-size: cover; 
                                                            border-radius: 5px; 
                                                            text-decoration: none; 
                                                            background: #095483; 
                                                            background-image: url(img/mercury/logo-mercury.svg);
                                                            background-position: left;
                                                            background-repeat: no-repeat;
                                                            display: flex;
                                                            align-items: center;
                                                            justify-content: flex-end;
                                                            color:  white;
                                                            width: 83px;
                                                            height: 32px;">
                                                    Mecury Gate&lt;/div&gt;
                                                    &lt;/a&gt;
                                            </code>
                                        </div>
                                        <div class="box-code-2">
                                            <code>
                                                &lt;a href="#"&gt;
                                                &lt;div 
                                                    style="background-size: cover; 
                                                            border-radius: 5px; 
                                                            text-decoration: none; 
                                                            background: #095483; 
                                                            background-image: url(img/mercury/logo-mercury.svg);
                                                            background-position: left;
                                                            background-repeat: no-repeat;
                                                            display: flex;
                                                            align-items: center;
                                                            justify-content: flex-end;
                                                            color:  white;
                                                            width: 126px;
                                                            height: 48px;">
                                                    Mecury Gate&lt;/div&gt;
                                                    &lt;/a&gt;
                                            </code>
                                        </div>
                                        <div class="box-code-3">
                                            <code>
                                                &lt;a href="#"&gt;
                                                &lt;div 
                                                    style="background-size: cover; 
                                                            border-radius: 5px; 
                                                            text-decoration: none; 
                                                            background: #095483; 
                                                            background-image: url(img/mercury/logo-mercury.svg);
                                                            background-position: left;
                                                            background-repeat: no-repeat;
                                                            display: flex;
                                                            align-items: center;
                                                            justify-content: flex-end;
                                                            color:  white;
                                                            width: 168px;
                                                            height: 64px;">
                                                    Mecury Gate&lt;/div&gt;
                                                    &lt;/a&gt;
                                            </code>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-4">
                                     <h3>PREVIEW BUTTON</h3>
                                     <div class="preview-button btn-preview-1 ">
                                        <a href="http://localhost/mercurygatefe/payments_tools/choose_currency.php"><img src="../img/button-checkout-146x57-mercury-gate.png"></a>
                                        <h3>Copy Code</h3>
                                        <a href=""><div class="copy-code"></div></a>
                                     </div>
                                     <div class="preview-button btn-preview-2 ">
                                        <a href="http://localhost/mercurygatefe/payments_tools/choose_currency.php"><img src="../img/button-checkout-168x65-mercury-gate.png"></a>
                                        <h3>Copy Code</h3>
                                        <a href=""><div class="copy-code"></div></a>
                                     </div>
                                     <div class="preview-button btn-preview-3 ">
                                        <a href="http://localhost/mercurygatefe/payments_tools/choose_currency.php"><img src="../img/button-checkout-210x82-mercury-gate.png"></a>
                                        <h3>Copy Code</h3>
                                        <a href=""><div class="copy-code"></div></a>
                                     </div>
                                </div>
                            </div>
                        </div>             
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>