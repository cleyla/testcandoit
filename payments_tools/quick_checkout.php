<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Quick Checkout</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-payments-tools quick-check-out">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-description">
                        <h3>quick checkout for web</h3>
                        <p>My Quick Checkout URL</p>
                        <span>This URL is safe to bookmark.</span>
                        <div class="btn-quick-checkout-general">
                            <a href="http://localhost/mercurygatefe/payments_tools/create_invoice.php" class="btn-quick-checkout">QUICK CHECKOUT</a>
                        </div>
                        <div class="view-tip-report">
                            <span>A summary of orders and gratuity is available on your BitPay Tip Report.</span>
                            <div class="btn-view-tip">
                                <a href="http://localhost/mercurygatefe/payments_tools/quick_checkout_tip_report.php">View tip report</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-quick-check-out">
                        <div class="title-right">
                            <h3>Customize quick checkout</h3>
                        </div>
                        <div class="input-inline">
                            <label>Merchant name</label>
                            <input class="input-medium" type="text" placeholder="backlayer Inc">
                        </div>
                        <div class="dropdown-inline">
                            <label>Default Currency</label>
                            <button class="dropdown-toggle input-medium" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                USD- US Dollar
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Option #1</a>
                                <a class="dropdown-item" href="#">Option #2</a>
                                <a class="dropdown-item" href="#">Option #3</a>
                                <a class="dropdown-item" href="#">Option #4</a>
                            </div>
                        </div>
                        <div class="input-inline">
                            <label>Display Addional</label>
                            <input class="input-medium" type="text" placeholder="EUR - Eurozone Euro">
                        </div>
                        <div class="title-form">
                            <h3>Payment Notifications</h3>
                        </div>
                        <div class="input-inline">
                            <label>Email confirmation to</label>
                            <input class="input-large type="email" placeholder="mp@backlayer.com">
                        </div>
                        <div class="input-inline">
                        <button type="button" class="icon-help" data-toggle="tooltip" data-html="true" data-placement="right" title="The URL on your server to receive IPN (Instant Payment Notification) when the invoice status changes"></button>
                            <label>Server IPN</label>
                            <input class="input-large" type="number" placeholder="Server IPN (optional)">
                        </div>
                        <div class="btn-save-changes">
                            <a href="http://localhost/mercury_gate/new_bill.php">Save Changes</a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>      


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>