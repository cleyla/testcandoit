<div class="topbar-mg">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="logo-mercury-gate">
					<img src="img/logo-white-mercury-gate.png" alt="Logo Mercury Gate" title="Logo Mercury Gate">
				</div>
			</div>
			<div class="col-md-6">
				<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-setting">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  	</button>

			  		<div class="collapse navbar-collapse options-settings" id="navbarSupportedContent">
			    		<ul class="navbar-nav ml-auto">
						 <li>
							  <button class="switchMC">
							  	<span class="iconMCWhite"></span>
								 	switch to
									mercury.cash
							  </button>
							  </li>
			    			<li class="nav-item dropdown lang-options">
				        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				        			EN
				        		</a>
				        		<div class="dropdown-menu drop-general drop-lang" aria-labelledby="navbarDropdown">
				        			<a  class=" dropdown-item bg-title-dropdown">
				        				Language
				        			</a>
				          			<a class="dropdown-item" href="#">
				          				<div class="icon-flag-en"></div>
				         				<div class="title-dropdown"> English</div>
				          			</a>
				          			<a class="dropdown-item" href="#">
				          				<div class="icon-flag-es"></div>
				         				<div class="title-dropdown"> Spanish</div>
				          			</a>
						          	<a class="dropdown-item" href="#">
						          		<div class="icon-flag-est"></div>
						         		<div class="title-dropdown"> Estonian</div>
						          	</a>
				        		</div>
			      			</li>
			      			<li class="nav-item dropdown li-settings ">
			      				<div class="number-notifications">4</div>
						        <a class="nav-link dropdown-toggle drop-settings" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						        	<img src="img/icon-notifications-mercury-gate.png">
						        </a>
						        <div class="dropdown-menu drop-general d-notifications" aria-labelledby="navbarDropdown">
						        	<a  class=" dropdown-item bg-title-dropdown">Notifications</a>
						         	<a class="dropdown-item" href="#">
						         		<div class="icon-gif"></div>
						         		<div class="title-dropdown">Victor sent you a<br> new donation</div>
						         	</a>
						         	<a class="dropdown-item" href="#">
						         		<div class="icon-new-payments"></div>
						         		<div class="title-dropdown">New payment from<br>Name Company</div>
						         	</a>
						         	<a class="dropdown-item" href="#">
						         		<div class="icon-gif"></div>
						         		<div class="title-dropdown">New Payment from<br>John Doe</div>
						         	</a>
						         	<a class="dropdown-item" href="#">
						         		<div class="icon-new-payments"></div>
						         		<div class="title-dropdown">UserName sent you a<br>new donation</div>
						         	</a>
						        </div>
			      			</li>
			      			<li class="nav-item dropdown li-settings ">
				        		<a class="nav-link dropdown-toggle drop-profile-a" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				        			<img src="img/photo-settings-mercury-gate.png">
				        		</a>
						        <div class="dropdown-menu drop-general drop-profile" aria-labelledby="navbarDropdown">
						        	<a  class=" dropdown-item bg-title-dropdown">Settings</a>
						         	<a class="dropdown-item" href="#">
						         		<div class="icon-profile"></div>
						         		<div class="title-dropdown"> Edit Profile</div>
						         	</a>
						         	<a class="dropdown-item" href="#">
						         		<div class="icon-logout"></div>
						         		<div class="title-dropdown">Log Out</div>
						         	</a>
						        </div>
			      			</li>
							 
			    		</ul>
			  		</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
				  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    		<span class="navbar-toggler-icon"></span>
		  			</button>
		  			<div class="collapse navbar-collapse" id="navbarSupportedContent">
					    <ul class="navbar-nav mr-auto">
					    	<li class="nav-item active">
					    		<div class="nav-dashboard nav-icons"></div>
					        	<a class="nav-link" href="#">Dashboard <span class="sr-only">(current)</span></a>
					      	</li>
					      	<li class="nav-item">
					      		<div class="nav-payments nav-icons"></div>
					        	<a class="nav-link" href="#">Payments</a>
					      	</li>
					      	<li class="nav-item">
					      		<div class="nav-payments-tools nav-icons"></div>
								<a class="nav-link" href="#">Payment Tools</a>
					      	</li>
					      	<li class="nav-item">
					      		<div class="nav-settings nav-icons"></div>
								<a class="nav-link nav-margin" href="#">Settings</a>
					      	</li>

					    </ul>
		  			</div>
				</nav>
			</div>
		</div>
	</div>

</header>