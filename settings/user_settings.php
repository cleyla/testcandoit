<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - User Settings</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-user-settings">
            <div class="row">
                <div class="col-12 col-md-3 col-xl-3">
                    <div class="btn-general">
                        <a href="#" class="btn-documentation">Documentation</a>
                        <a href="#" class="btn-help">Help + support</a>
                    </div>
                </div>
                <div class="col-12 col-md-9 col-xl-9">
					<div class="btn-user-settings">
						<h3>USER SETTINGS</h3>
						<div class="row">
							<div class="col-xl-5 col-lg-5">
								<div class="box-btn btn-1">
									<a href="http://localhost/mercurygatefe/settings/basic_info.php">
										<h5>BASIC</h5>
										<div class="info-basic">
											<ul class="ul-1">
												<li>Name</li>
												<li>Phone</li>
											</ul>
											<ul class="ul-2">
												<li>Marco Pirrongelli</li>
												<li>None</li>
											</ul>
										</div>
									</a>
								</div>
							</div>
							<div class="col-xl-5 col-lg-5">
								<div class="box-btn btn-2">
									<a href="http://localhost/mercurygatefe/settings/user_security.php">
										<h5>Security</h5>
										<p><strong>Two-factor Security</strong> Enter token to <br>enable two-factor authentication</p>
									</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-5 col-lg-5">
								<div class="box-btn btn-3">
									<a href="http://localhost/mercurygatefe/settings/change_password.php">
										<h5>PASSWORD</h5>
										<p>Update or modify your password account</p>
									</a>
								</div>
							</div>
							<div class="col-xl-5 col-lg-5">
								<div class="box-btn btn-4">
									<a href="http://localhost/mercurygatefe/settings/night_mode.php">
										<h5>Night Mode</h5>
										<p>You can set a shedule for when<br> you wan to use night mode</p>
										<div class="custom-control custom-switch">
											<span>Turn on now</span>
											<input type="checkbox" class="custom-control-input" id="autoConvert" checked="checked">
											<label class="custom-control-label" for="autoConvert"></label>
	                        			</div>
									</a>
								</div>
							</div>
						</div>
						<!-- <div class="col-xl-5">
							<a href="#">
								<div class="box-btn btn-2">
									<h5>SECURITY</h5>
									<p><strong>Two-Factor Security</strong><br>Enter token to enable two-factor authentication</p>
								<div>
							</a>
						</div> -->

					</div> <!--btn-user-settings -->                                   
                </div>
            </div>
        </div>
    </div>
  


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>