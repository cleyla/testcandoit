<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Mercury Gate - Wallet Settings</title>
        <meta charset="UTF-8">
        <meta
            name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
        <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

        <!-- Bootstrap CSS -->
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous">
        <!-- DataTables -->
        <link
            rel="stylesheet"
            href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
            rel="stylesheet"/>
        <!-- Font awesome -->
        <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
            integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
            crossorigin="anonymous">
    </head>
    <body>

        <?php include('header.php'); ?>
        <div class="container-general">
            <div class="container-settings">
                <h3>Wallet Settings</h3>
                <div class="row mt-5 mt-sm-5">
                    <div class="col-lg-3">
                        <div class="row content-left">
                            <div class="col-4 col-lg-12 mt-3 mt-lg-4">
                                <p class="title-cl">Active</p>
                                <p class="value-cl">1</p>
                            </div>
                            <div class="col-4 col-lg-12 mt-3 mt-lg-5">
                                <p class="title-cl">Inactive</p>
                                <p class="value-cl">0</p>
                            </div>
                            <div class="col-4 col-lg-12 mt-3 mt-lg-5">
                                <p class="title-cl">Settlenment%</p>
                                <p class="value-cl">100%</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 table-responsive form-table00">
                        <div class="form-row form-right form-table00-head">
                            <div class="editfile"></div>
                            <div class="col-2">
                                <label>Nickname</label>
                            </div>
                            <div class="col-4">
                                <label>Address</label>
                            </div>
                            <div class="col-2">
                                <label>Currency</label>
                            </div>
                            <div class="col-2">
                                <label>Balance</label>
                            </div>
                            <div class="col-2">
                                <label>Auto-Convert</label>
                            </div>
                        </div>
                            <!--contenido del formulario-->
                            <div class="form-row form-right form-table00-body">
                                <div class="col-2">
                                    <div class="container-checkbox check-convert">
                                        <div class="container-switch">
                                            <input type="checkbox" name="switchCR" id="switchCR" class="checkbox-switch" checked="checked">
                                            <label for="switchCR" class="switch"> </label>
                                        </div>
                                        <span class="span-legend">Bitcoin</span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <span class="settings-address">343Ehh8ZStEAhBvX6N1VhKZrdMpjuwHecV</span>
                                </div>
                                <div class="col-2">
                                    <img class="icon-cripto position-relative settings-currency" src="../img/logo-bitcoin-26x25-mercury-gate.png" alt="BTC">
                                    <span class="text-uppercase">BTC</span>
                                </div>
                                <div class="col-2">
                                    <span>0.000000 <span class="text-uppercase">BTC</span></span>
                                </div>
                                <div class="col-2">
                                    <div class="container-checkbox check-convert">
                                        <div class="container-switch">
                                            <input type="checkbox" name="switch" id="switch" class="checkbox-switch" checked="checked">
                                            <label for="switch" class="switch"> </label>
                                        </div>
                                        <span class="span-legend">disable</span>
                                    </div>
                                </div>
                            </div>
                        <!--contenido del formulario-->
                            <!--contenido del formulario-->
                            <div class="form-row form-right form-table00-body">
                                <div class="col-2">
                                    <div class="container-checkbox check-convert">
                                        <div class="container-switch">
                                            <input type="checkbox" name="switchCR" id="switchCR" class="checkbox-switch" checked="checked">
                                            <label for="switchCR" class="switch"> </label>
                                        </div>
                                        <span class="span-legend">Ethereum</span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <span class="settings-address">343Ehh8ZStEAhBvX6N1VhKZrdMpjuwHecV</span>
                                </div>
                                <div class="col-2">
                                    <img class="icon-cripto position-relative settings-currency" src="../img/logo-ethereum-26x25-mercury-gate.png" alt="BTC">
                                    <span class="text-uppercase">ETH</span>
                                </div>
                                <div class="col-2">
                                    <span>0.000000 <span class="text-uppercase">ETH</span></span>
                                </div>
                                <div class="col-2">
                                    <div class="container-checkbox check-convert">
                                        <div class="container-switch">
                                            <input type="checkbox" name="switch" id="switch" class="checkbox-switch" checked="checked">
                                            <label for="switch" class="switch"> </label>
                                        </div>
                                        <span class="span-legend">disable</span>
                                    </div>
                                </div>
                            </div>
                        <!--contenido del formulario-->
                            <!--contenido del formulario-->
                            <div class="form-row form-right form-table00-body">
                                <div class="col-2">
                                    <div class="container-checkbox check-convert">
                                        <div class="container-switch">
                                            <input type="checkbox" name="switchCR" id="switchCR" class="checkbox-switch" checked="checked">
                                            <label for="switchCR" class="switch"> </label>
                                        </div>
                                        <span class="span-legend">Dash</span>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <span class="settings-address">343Ehh8ZStEAhBvX6N1VhKZrdMpjuwHecV</span>
                                </div>
                                <div class="col-2">
                                    <img class="icon-cripto position-relative settings-currency" src="../img/logo-dash-26x25-mercury-gate.png" alt="BTC">
                                    <span  class="text-uppercase">BTC</span>
                                </div>
                                <div class="col-2">
                                    <span>0.000000 <span class="text-uppercase">DASH</span></span>
                                </div>
                                <div class="col-2">
                                    <div class="container-checkbox check-convert">
                                        <div class="container-switch">
                                            <input type="checkbox" name="switch" id="switch" class="checkbox-switch" checked="checked">
                                            <label for="switch" class="switch"> </label>
                                        </div>
                                        <span class="span-legend">disable</span>
                                    </div>
                                </div>
                            </div>
                        <!--contenido del formulario-->
                    </div>
                </div>
            </div>
        </div>

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!-- Select 2 -->
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="../js/settings-order.js"></script>

</body>

</html>