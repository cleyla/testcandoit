<h3>Add Currency</h3>
<div class="row mt-5 mt-sm-5">
    <div class="col-lg-3">
        <div class="content-left">
            <p class="title-cl">Currency and Location</p>
            <span class="description-cl">Select the currency you'd like to receive in your
                settlement and the country of your bank location.</span>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-row form-right">
            <div class="col-lg-4 pl-0 pl-lg-4 mt-3 mt-md-0">
                <label>Choose Currency</label>
                <div class="dropdown dropdown-mg">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose Currency
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Bitcoin - BTC</a>
                        <a class="dropdown-item" href="#">Ethereum - ETH</a>
                        <a class="dropdown-item" href="#">Dash - DASH</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="btn-mg-save fixed-btn">
            <a href="#" class="btn-save">Save</a>
        </div>
    </div>