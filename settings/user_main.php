<h3>Users</h3>
<div class="row mt-4 mt-sm-5">
    <div class="col-lg-3">
        <div class="content-left content-user-left">
            <span class="title-cl">You are the only user</span>
                <span class="description-cl">
                    You are the only user and have full administrative capabilities. 
                    Invite more users to your organization.
                </span>
            </span>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-row form-right h-100 d-flex align-items-center">
            <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-lg-2">
                <div class="btn-mg">
                    <a href="#" class="btn-invite ml-auto ml-sm-0">Invite User</a>
                </div>
            </div>
        </div>
    </div>
</div>