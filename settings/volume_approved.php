<h3>Approved Volume</h3>
<div class="row mt-4 mt-sm-4">
    <div class="col-lg-3">
        <div class="content-left">
            <span class="description-cl">In order to raise your daily processing limits, we
                require additional information to verify your identity and your business. You
                can apply to raise your approved sales volume at any time.
                <span class="mt-2 mt-sm-5 d-block">
                    To process more than $100,000/day, please contact
                    <br>
                    <a href="mailto:support@mercury.cash"  class="link-mg">mercury.cash</a>
                </span>
            </span>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-row form-right">
            <div class="col-md-10 pl-0 pl-md-4 mt-3 mt-lg-2">
                <label>Tier 2 - Up to $10.000 daily, $500,000 annually</label>
                <div class="btn-mg">
                    <a href="#" class="btn-verified ml-auto ml-sm-0">Verified</a>
                </div>
            </div>
            <br>
            <div class="col-md-10 pl-0 pl-md-4 mt-3 mt-lg-4">
                <label>Tier 3 - Up to $10.000 daily, $5,000,000 annually</label>
                <div class="btn-mg">
                    <a href="#" class="btn-apply ml-auto ml-sm-0">Apply</a>
                </div>
            </div>
        </div>
    </div>
</div>