<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Mercury Gate - Order Settings</title>
        <meta charset="UTF-8">
        <meta
            name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
        <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

        <!-- Bootstrap CSS -->
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous">

        <!-- Font awesome -->
        <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
            integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
            crossorigin="anonymous">
    </head>
    <body>
        <?php include('header.php'); ?>
        <div class="container-general">
            <div class="container-settings">
                <h3>Order Settings</h3>
                <div class="row mt-5 mt-sm-5">
                    <div class="col-lg-3">
                        <div class="content-left">
                            <p class="title-cl">Payment Notification</p>
                            <span class="description-cl">When invoices are paid, we send notifications via
                                email and/or web hook. You may choose a specific email at which to receive these
                                notifications or choose not to receive them at all.</span>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="form-row form-right mt-2 mt-md-5">
                            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                                <label class="container-radio">Send notifications to mp@backlayer.com
                                    <input type="radio" name="radio" value="option00">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                                <label class="container-radio">Send notifications to a different email
                                    <input id="addfile" type="radio" name="radio" value="option01">
                                    <span class="checkmark"></span>
                                </label>
                                <div
                                    id="newfile"
                                    class="col-md-5 pl-0 pl-md-2 mb-3 mb-md-4 ml-0 ml-md-4 mt-4 mt-md-3">
                                    <input type="text" class="form-control" placeholder="backlayer Inc">
                                </div>
                            </div>
                            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                                <label class="container-radio">Do not send me notifications via email
                                    <input type="radio" name="radio" value="option02">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 mt-sm-5">
                    <div class="col-lg-3">
                        <div class="content-left">
                            <p class="title-cl">Transaction Speed & Risk</p>
                            <span class="description-cl d-block">This setting changes the length of time we
                                wait before notifying you of a payment. The medium setting is recommended for
                                most merchants as it is considered safe for most transactions. On average,
                                medium speed payments are confirmed in 10 minutes.
                            </span>
                            <span class="description-cl d-block">
                                If you want an immediate notification for a payment, you can use the high speed
                                setting. However, this setting makes you more susceptible to receiving
                                fraudulent payments, so it is not recommended. Regardless of this setting, a
                                fully paid invoice is credited to your account only after the transaction has
                                accrued the number of confirmations at which mercurygate considers the payment
                                fully settled.
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="form-row form-right mt-4 mt-lg-0">
                            <p class="title-cr pl-0 pl-md-4">Product / Comments</p>
                            <div class="col-md-8 pl-0 pl-md-4">
                                <label class="container-radio">Invoice will be confirmed immediately when payment is received on the network.
                                    <input type="radio" name="radioPC" value="PC00">
                                    <span class="checkmark"></span>
                                </label>
                                <div class="subcontentradio ml-2 ml-md-4 mt-3">
                                    <label class="container-radio">This is recommended for most organizations. By
                                        turning this off, I want all my transactions accepted at High speed, even if
                                        they are higher risk.
                                        <input type="radio" name="radioPC" value="PC00-1">
                                        <span class="checkmark"></span>
                                        <small class="d-block title-radio mt-2 mt-md-3">If MercuryGate determines that an incoming Payment is A potential
                                             Double-spend or has a higher risk or for some reason has not being confirmed, I would like to Mercury Gate to 
                                             downgrade the speed of this transaction from high to medium and i will not be notified until the transaction
                                             has one confirmation in the blockchain.
                                        </small>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8 pl-0 pl-md-4 mt-3 mt-md-0">
                                <label class="container-radio">Medium (Recommended for most merchants) Invoice
                                    will be confirmed when full payment has been confirmed by one block on the
                                    network.
                                    <input type="radio" name="radioPC" value="PC01">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="col-md-8 pl-0 pl-md-4 mt-3 mt-md-0">
                                <label class="container-radio">Invoice will be confirmed when full payment has
                                    been confirmed to the confirmation number at which mercurygate considers it
                                    fully settled.
                                    <input type="radio" name="radioPC" value="PC02">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5 mt-sm-5">
                    <div class="col-lg-3">
                        <div class="content-left">
                            <p class="title-cl">Refund Exchange Rate Policy</p>
                            <span class="description-cl d-block">As the price of bitcoin can fluctuate, it’s
                                important that bitcoin-accepting merchants maintain a clear refund policy.
                            </span>
                            <span class="description-cl d-block">
                                We recommend the “Current Rate Refunds” policy, which does not expose merchants
                                to bitcoin price changes occurring between the time the payment is made and the
                                time the refund is issued.
                            </span>
                        </span>
                        <span class="description-cl d-block">
                            While the refund policy determines the method by which bitcoin refund amounts
                            are calculated, Mercury Gate enables merchants to issue full or partial refunds for
                            all transactions.
                        </span>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="form-row form-right mt-4 mt-lg-0">
                        <div class="col-md-8 pl-0 pl-md-4">
                            <label class="container-radio">
                                <span class="title-radio d-block mb-2">
                                    Current Rate Refunds (recommended)
                                </span>
                                The refunded bitcoin amount will be calculated using the BBB rate at the time
                                the refund is made. The precise bitcoin amount will likely be different than the
                                original amount sent, but it’s value will be equivalent in the pricing currency.
                                <input type="radio" name="radioCRR">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-md-8 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label class="container-radio">
                                <span class="title-radio d-block mb-2">
                                    Fixed Rate Refunds
                                </span>
                                <span>The refunded bitcoin amount will be calculated using the BBB rate at the
                                    time of purchase. Fixed Rate Refunds may cost a merchant more to issue than the
                                    original value of the payment, and this policy is not recommended for merchants
                                    without other means of hedging bitcoin price volatility.
                                </span>
                                <input type="radio" name="radioCRR">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="btn-mg-save">
                    <a href="#" class="btn-save">Save</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <script src="../js/settings-order.js"></script>

</body>

</html>