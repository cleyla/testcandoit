<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - Merchant Profile</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('header.php'); ?>
    <div class="container-general">
        <div class="container-settings">              
          <h3>Merchant Profile</h3>
            <div class="row mt-5 mt-sm-5">
                <div class="col-md-3">
                    <div class="content-left">
                      <p class="title-cl">Organization info</p>
                    <span class="description-cl">If you need to update any locked organization fields please contact mercury.cash</span>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-row form-right">
                      <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                        <label>Name</label>
                        <input type="text" class="form-control" placeholder="Name">
                      </div>
                      <div class="col-md-6 pl-0 pl-md-4 mt-3 mt-md-0">
                        <label>Description</label>
                        <input type="text" class="form-control" placeholder="Hosting 10GB">
                      </div>
                      <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                        <label>Industry</label>
                        <input type="text" class="form-control" placeholder="Information & Technology">
                      </div>
                      <div class="col-md-5 pl-0 pl-md-4 mt-mg-cs">
                        <label>Website</label>
                        <input type="text" class="form-control" placeholder="www.backlayer.com">
                      </div>
                    </div>
                </div>
            </div>
             <div class="row mt-5 mt-sm-5">
                <div class="col-md-3">
                    <div class="content-left">
                    <p class="title-cl">Support Contact</p>
                    <span  class="description-cl">Providing support contact information during the checkout process can improve
                      conversion rates and customer satisfaction. Contact information provided here will be
                      displayed on your Mercury Gate invoices and receipts.</span>
                </div>
                </div>
                <div class="col-md-9">
                    <div class="form-row form-right">
                      <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                        <label>Support Phone</label>
                        <input type="text" class="form-control" placeholder="9543882288">
                      </div>
                      <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                        <label>Preferred Contact Method</label>
                        <div class="container-checkbox">
                          <span class="span-legend">Email</span>
                            <div class="container-switch">
                              <input type="checkbox" name="switch" id="switch" class="checkbox-switch" checked="checked">
                              <label for="switch" class="switch"> </label>
                            </div>
                          <span class="span-legend">Website</span>
                        </div>
                      </div>
                      <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0 d-none support-website">
                        <label>Support Website</label>
                        <input type="text" class="form-control" placeholder="write website">
                      </div>
                      <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0 support-email">
                        <label>Support Email</label>
                        <input type="text" class="form-control" placeholder="write email">
                      </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5 mt-sm-5">
                <div class="col-md-3">
                    <div class="content-left">
                    <p class="title-cl">Retail Location</p>
                </div>
                </div>
                <div class="col-md-9">
                      <div class="col-md-6 pl-0 pl-md-4 mt-3 mt-md-0">
                        <label class="label00">Do you accept bitcoin at a retail location?</label>
                        <div class="container-checkbox">
                          <span class="span-legend">No</span>
                            <div class="container-switch">
                              <input type="checkbox" name="switchRL" id="switchRL" class="checkbox-switch" checked="checked">
                              <label for="switchRL" class="switch"> </label>
                            </div>
                          <span class="span-legend">Yes</span>
                        </div>
                      </div>
                      <div class="form-right contentRL d-none">
                        <div class="row mt-2 mt-sm-4">
                          <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label>Country</label>
                            <select class="js-example-basic-single form-control" style="width: 100%" name="country">
                                <option value="" selected="selected">Country Name</option> <!--Placeholder del select -->
                                  <option value="United States">United States</option> 
                                  <option value="United Kingdom">United Kingdom</option> 
                                  <option value="Afghanistan">Afghanistan</option> 
                                  <option value="Albania">Albania</option> 
                                  <option value="Algeria">Algeria</option> 
                                  <option value="American Samoa">American Samoa</option> 
                                  <option value="Andorra">Andorra</option> 
                                  <option value="Angola">Angola</option> 
                                  <option value="Anguilla">Anguilla</option> 
                                  <option value="Antarctica">Antarctica</option> 
                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                  <option value="Argentina">Argentina</option> 
                                  <option value="Armenia">Armenia</option> 
                                  <option value="Aruba">Aruba</option> 
                                  <option value="Australia">Australia</option> 
                              </select>
                          </div>
                           <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label>State / Province</label>
                            <select class="js-example-basic-single form-control select-state" style="width: 100%" name="state">
                                 <option value="" selected="selected">State Name</option><!--Placeholder del select -->
                                  <option value="state">state</option>  
                              </select>
                          </div>
                           <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label>City</label>
                            <select class="js-example-basic-single form-control select-city" style="width: 100%" name="city">
                                <option value="" selected="selected">City Name</option> <!--Placeholder del select -->
                                  <option value="city">city</option> 
                              </select>
                          </div>
                          <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                            <label>Address Line 1</label>
                            <input type="text" class="form-control" placeholder="Address Line 1">
                          </div>
                          <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                            <label>Address Line 2</label>
                            <input type="text" class="form-control" placeholder="Write Your Line 2">
                          </div>
                          <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                            <label>ZIP / Postal Code</label>
                            <input type="text" class="form-control" placeholder="123456789">
                          </div>
                        </div>
                      </div>
                </div>
            </div>
            <div class="row mt-5 mt-sm-5">
                <div class="col-md-3">
                    <div class="content-left">
                    <p class="title-cl">Invoice Location</p>
                </div>
                </div>
                  <div class="col-md-9">
                      <div class="col-md-6 pl-0 pl-md-4 mt-3 mt-md-0">
                        <label class="label00">Do you accept bitcoin at a retail location?</label>
                        <div class="container-checkbox">
                          <span class="span-legend">No</span>
                            <div class="container-switch">
                              <input type="checkbox" name="switchIL" id="switchIL" class="checkbox-switch" checked="checked">
                              <label for="switchIL" class="switch"> </label>
                            </div>
                          <span class="span-legend">Yes</span>
                        </div>
                      </div>
                      <div class="form-right contentIL d-none">
                        <div class="row mt-2 mt-sm-4">
                          <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label>Country</label>
                            <select class="js-example-basic-single form-control" style="width: 100%" name="country">
                                <option value="" selected="selected">Country Name</option> <!--Placeholder del select -->
                                  <option value="United States">United States</option> 
                                  <option value="United Kingdom">United Kingdom</option> 
                                  <option value="Afghanistan">Afghanistan</option> 
                                  <option value="Albania">Albania</option> 
                                  <option value="Algeria">Algeria</option> 
                                  <option value="American Samoa">American Samoa</option> 
                                  <option value="Andorra">Andorra</option> 
                                  <option value="Angola">Angola</option> 
                                  <option value="Anguilla">Anguilla</option> 
                                  <option value="Antarctica">Antarctica</option> 
                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                  <option value="Argentina">Argentina</option> 
                                  <option value="Armenia">Armenia</option> 
                                  <option value="Aruba">Aruba</option> 
                                  <option value="Australia">Australia</option> 
                              </select>
                          </div>
                           <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label>State / Province</label>
                            <select class="js-example-basic-single form-control select-state" style="width: 100%" name="state">
                                <option value="" selected="selected">State Name</option> <!--Placeholder del select -->
                                  <option value="United States">United States</option> 
                                  <option value="United Kingdom">United Kingdom</option> 
                                  <option value="Afghanistan">Afghanistan</option> 
                                  <option value="Albania">Albania</option> 
                                  <option value="Algeria">Algeria</option> 
                                  <option value="American Samoa">American Samoa</option> 
                                  <option value="Andorra">Andorra</option> 
                                  <option value="Angola">Angola</option> 
                                  <option value="Anguilla">Anguilla</option> 
                                  <option value="Antarctica">Antarctica</option> 
                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                  <option value="Argentina">Argentina</option> 
                                  <option value="Armenia">Armenia</option> 
                                  <option value="Aruba">Aruba</option> 
                                  <option value="Australia">Australia</option> 
                              </select>
                          </div>
                           <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                            <label>City</label>
                            <select class="js-example-basic-single form-control select-city" style="width: 100%" name="city">
                                <option value="" selected="selected">City Name</option> <!--Placeholder del select -->
                                  <option value="United States">United States</option> 
                                  <option value="United Kingdom">United Kingdom</option> 
                                  <option value="Afghanistan">Afghanistan</option> 
                                  <option value="Albania">Albania</option> 
                                  <option value="Algeria">Algeria</option> 
                                  <option value="American Samoa">American Samoa</option> 
                                  <option value="Andorra">Andorra</option> 
                                  <option value="Angola">Angola</option> 
                                  <option value="Anguilla">Anguilla</option> 
                                  <option value="Antarctica">Antarctica</option> 
                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option> 
                                  <option value="Argentina">Argentina</option> 
                                  <option value="Armenia">Armenia</option> 
                                  <option value="Aruba">Aruba</option> 
                                  <option value="Australia">Australia</option> 
                              </select>
                          </div>
                          <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                            <label>Address Line 1</label>
                            <input type="text" class="form-control" placeholder="Address Line 1">
                          </div>
                          <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                            <label>Address Line 2</label>
                            <input type="text" class="form-control" placeholder="Write Your Line 2">
                          </div>
                          <div class="col-md-4 pl-0 pl-md-4 mt-mg-cs">
                            <label>ZIP / Postal Code</label>
                            <input type="text" class="form-control" placeholder="123456789">
                          </div>
                        </div>
                      </div>
                </div>
              </div>
            <div class="row">
              <div class="btn-mg-save">
                <a href="#" class="btn-save">Save</a>
            </div>
            </div>
        </div>
    </div>
  


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/settings.js"></script>

 
  </body>

</html>