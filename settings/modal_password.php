<!-- Modal -->
<div class="modal fade" id="changePass" tabindex="-1" role="dialog" aria-labelledby="changePassLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <img src="../img/icon-delete-mercury-gate-20x20.png">
        <h5>Confirm Delete Bill</h5>
        <p>Please type the buyer's email address "m.pirro1987@gmail.com" to confirm that you want to delete their bill.</p>
        <h6>Buyer's email address</h6>
        <input type="email" placeholder="write the email address">
      </div>
      <div class="modal-footer">
        <div class="btn-footer">
            <button type="button" class="">Delete bill</button>
        </div>
      </div> -->
      <ul class="nav nav-tabs nav-hide" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link nav-custom active" id="update_pass_1_tab" data-toggle="tab" href="#update_pass_1" role="tab" aria-controls="update_pass_1" aria-selected="true">update_pass_1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link nav-custom" id="update_pass_2_tab" data-toggle="tab" href="#update_pass_2" role="tab" aria-controls="update_pass_2" aria-selected="false">update_pass_2</a>
        </li>
        <li class="nav-item">
          <a class="nav-link nav-custom" id="update_pass_3_tab" data-toggle="tab" href="#update_pass_3" role="tab" aria-controls="update_pass_3" aria-selected="false">update_pass_3</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="update_pass_1" role="tabpanel" aria-labelledby="update_pass_1_tab">
            <div class="icon-password"></div>
            <h5>Update Password</h5>
            <p>To update your password, the change will be applied to your mercury cash user</p>    
            <div class="btn-footer">
                <a href="#" class="nexttab btn-modal">Continue</a>
                <a href="#" class="btn-modal" data-dismiss="modal">Cancel</a>
            </div>
        </div>
        <div class="tab-pane fade" id="update_pass_2" role="tabpanel" aria-labelledby="update_pass_2_tab">
            <div class="icon-password"></div>
            <h5>Write the verification code</h5>
            <p>From the google authenticator app</p>
            <input type="number" placeholder="888888">    
            <div class="btn-footer">
                <a href="#" class="nexttab btn-modal">Verify</a>
                <a href="#" class="btn-modal" data-dismiss="modal">Cancel</a>
            </div>
        </div>
        <div class="tab-pane fade" id="update_pass_3" role="tabpanel" aria-labelledby="update_pass_3_tab">
            <div class="icon-password"></div>
            <h5>Update Password</h5>
            <p>Your password has been<br> updated successfully</p>
            <div class="btn-footer">
                <a href="#" class="btn-modal" data-dismiss="modal">Continue</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>