<!DOCTYPE html>
<html lang="es">  
  <head>    
    <title>Mercury Gate - User Settings</title>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="../sass/main.css" rel="stylesheet" type="text/css"/>
    <link href="../sass/responsive.css" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--  Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  </head>  
  <body>

    <?php include('../header.php'); ?>
    <div class="container-general">
        <div class="container-user-settings">
            <div class="row">
                <div class="col-12 col-md-12 col-xl-3">
                    <div class="left-description">
                        <h3><a href="http://localhost/mercurygatefe/settings/user_settings.php">Night Mode</a></h3>
                        <div class="turn-now">
                            <h4>Turn on now</h4>
                            <div class="custom-control custom-switch">
								<input type="checkbox" class="custom-control-input" id="turnOn">
								<label class="custom-control-label" for="turnOn"></label>
	                        </div>
                            <p>You can set a shedule for when you want to use night mode. To use sunset to sunrise, location needs to be turned on.</p>
                        </div>
                        <div class="turn-shedule">
                            <h4>Turn on as sheduled</h4>
                            <div class="custom-control custom-switch">
								<input type="checkbox" class="custom-control-input" id="turnShedule">
								<label class="custom-control-label" for="turnShedule"></label>
                            </div>
                            <p>To prevent interruptions, night mode will wait until the screen in off before it turns on.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-xl-9">
					<div class="btn-user-settings">
                        <div class="options-shedule">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="sunsentSunrise" name="includeBuyer" value="turnOff" onChange="turnOnShedule(this.value);">
                                <label class="custom-control-label" for="sunsentSunrise">Sunset to sunrise</label>
                            </div> 
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="customShedule" name="includeBuyer" value="turnOn" onChange="turnOnShedule(this.value);">
                                <label class="custom-control-label" for="customShedule">Custom shedule</label>
                            </div>
                            <div id="options-custom" style="display:none">
                                <label>Start Time</label>
                                <input type="time" placeholder="00:00 PM">
                                <div class="input-inline">
                                    <label>End Time</label>
                                    <input type="time" placeholder="00:00 PM">
                                    <input type="date" placeholder="MONTH, DAY">
                                </div>
                            </div>
                        </div>
					</div> <!--btn-user-settings -->                                   
                </div>
            </div>
        </div>
    </div>
  


    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select 2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="../js/main.js"></script>
 
  </body>

</html>