                <h3>Settlements</h3>
                <div class="row mt-5 mt-sm-5">
                    <div class="col-lg-3">
                        <div class="row content-left">
                            <div class="col-3 col-lg-12 mt-3 mt-md-3">
                                <p class="title-cl">Active</p>
                                <p class="value-cl">1</p>
                            </div>
                            <div class="col-3 col-lg-12 mt-3 mt-md-3">
                                <p class="title-cl">Inactive</p>
                                <p class="value-cl">0</p>
                            </div>
                            <div class="col-6 col-lg-12 mt-3 mt-md-3">
                                <p class="title-cl">Settlenment%</p>
                                <p class="value-cl">100%</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 table-responsive form-table00">
                        <div class="form-row form-right form-table00-head">
                            <div class="editfile"></div>
                            <div class="col-3">
                                <label>Nickname</label>
                            </div>
                            <div class="col-5">
                                <label>Destination</label>
                            </div>
                            <div class="col-2">
                                <label>Currency</label>
                            </div>
                            <div class="col-2">
                                <label>Percentage</label>
                            </div>
                        </div>
                            <!--contenido del formulario-->
                            <div class="form-row form-right form-table00-body">
                                <div class="editfile"></div>
                                <div class="col-3">
                                    <input type="text" class="form-control" placeholder="BTC Coinbase">
                                </div>
                                <div class="col-5">
                                    <input type="text" class="form-control" placeholder="343Ehh8ZStEAhBvX6N1VhKZrdMpjuwHecV">
                                </div>
                                <div class="col-2 position-relative">
                                    <input type="text" class="form-control" placeholder="BTC">
                                    <img class="icon-cripto" src="../img/logo-bitcoin-26x25-mercury-gate.png" alt="BTC">
                                </div>
                                <div class="col-2">
                                    <div class="quantity">
                                        <input class="form-control" type="number" min="1" max="100" step="1" value="1">
                                    </div>
                                </div>
                            </div>
                        <!--contenido del formulario-->
                    </div>
                </div>
                <div class="row">
                    <div class="btn-mg btn-fixed-bottom">
                        <a href="#" class="btn-settlements">+ App settlements currency</a>
                    </div>
                </div>