<h3>Invite Users</h3>
<div class="row mt-5 mt-sm-5">
        <div class="col-md-3">
            <div class="content-left">
                <p class="title-cl">User Information</p>
                <span class="description-cl">
                    We  will send this user an email which they will confirm. Then they will be able to log into your dashboard.
                </span>
            </div>
        </div>
    <div class="col-md-9">
        <div class="form-row form-right">
            <div class="col-md-4 pl-0 pl-md-4 mt-3 mt-md-0">
                <label>Email</label>
                <input type="email" class="form-control" placeholder="write@yourmail.com"></div>
            <div class="col-md-6 pl-0 pl-md-4 mt-3 mt-md-0">
                <label>Full Name</label>
                <input type="text" class="form-control" placeholder="(optional)">
            </div>
        </div>
    </div>
</div>
<div class="row mt-5 mt-sm-5">
    <div class="col-lg-3">
        <div class="content-left">
            <p class="title-cl">Privileges</p>
            <span class="description-cl">You can check to assign privileges to a user. You can check more than one privilege.
            </span>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-row form-right mt-2 mt-md-0">
            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                <label class="container-radio">
                    <span>Admin</span>
                    <br>
                    Has full access to all features and settings
                    <input type="radio" name="radio" value="not00">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                <label class="container-radio">
                    <span>Accounting</span>
                    <br>
                    Can download the accounting ledger and initiate payments
                    <input id="addfile" type="radio" name="radio" value="not01">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                <label class="container-radio">
                    <span>Integration</span>
                    <br>
                    Can setup payment tools, manage API keys, and initiate payments
                    <input type="radio" name="radio" value="not02">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="col-md-12 pl-0 pl-md-4 mt-3 mt-md-0">
                <label class="container-radio">
                    <span>Support</span>
                    <br>
                    Can search for payments, and issue refunds
                    <input type="radio" name="radio" value="not03">
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="btn-mg btn-fixed-bottom">
            <a href="#" class="btn-send-invite">Send invite</a>
        </div>
    </div>