    <div class="row">
        <div class="col-md-6 col-lg-5 mt-2 mt-lg-5">
            <div class="card-outline">
                 <p class="card-out-title">Merchant Profile</p>
                <div class="card-out-body">
                    
                    <div class="row no-gutters">
                        <div class="col-sm-8 col-md-12 col-lg-8">
                                <div class="table-responsive card-out-table d-lg-flex">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Name</td>
                                                <td>Otto</td>
                                            </tr>
                                            <tr>
                                                <td>Industry</td>
                                                <td>Thornton</td>
                                            </tr>
                                            <tr>
                                                <td>Website</td>
                                                <td>Thornton</td>
                                            </tr>
                                            <tr>
                                                <td>Support Phone</td>
                                                <td>Thornton</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        <div class="col-sm-4 col-md-12 col-lg-4">
                            <div class="img-content">
                                <img src="../img/icon-settings-merchant-profile.png" class="img-fluid card-out-img mt-2 mt-lg-0" alt="Merchant Profile">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-5 mt-2 mt-lg-5">
            <div class="card-outline">
                <div class="card-out-body">
                    <p class="card-out-title">Approved Volume</p>
                    <div class="row no-gutters">
                        <div class="col-sm-8 col-md-12 col-lg-8">
                                <div class="table-responsive card-out-table d-lg-flex">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Tier</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>Daily</td>
                                                <td>$10.000</td>
                                            </tr>
                                            <tr>
                                                <td>Annual</td>
                                                <td>$5000.000.00</td>
                                            </tr>
                                            <tr>
                                                <td><a href class="link-mg">Increase Procesing Volume ></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        <div class="col-sm-4 col-md-12 col-lg-4">
                            <div class="img-content">
                                <img src="../img/icon-settings-approved-volume.png" class="img-fluid card-out-img mt-3 mt-lg-0" alt="Approved Volume">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-5 mt-2 mt-lg-5">
            <div class="card-outline">
                <div class="card-out-body">
                    <p class="card-out-title">Order Settings</p>
                    <div class="row no-gutters">
                        <div class="col-sm-8 col-md-12 col-lg-8">
                                <div class="table-responsive card-out-table d-lg-flex">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Speed</td>
                                                <td>Medium</td>
                                            </tr>
                                            <tr>
                                                <td>Email Notifications</td>
                                                <td>mp@baklayer.com</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        <div class="col-sm-4 col-md-12 col-lg-4">
                            <div class="img-content">
                                <img src="../img/icon-settings-order-settings.png" class="img-fluid card-out-img mt-2 mt-lg-0" alt="Order Settings">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-5 mt-2 mt-lg-5">
            <div class="card-outline">
                <div class="card-out-body">
                    <p class="card-out-title">Wallet Settings</p>
                    <div class="row no-gutters"> 
                        <div class="col-sm-8 col-md-12 col-lg-8">
                                <div class="table-responsive card-out-table d-lg-flex">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Actives</td>
                                                <td>1</td>
                                            </tr>
                                            <tr>
                                                <td>Inactives</td>
                                                <td>0</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        <div class="col-sm-4 col-md-12 col-lg-4">
                            <div class="img-content">
                                <img src="../img/icon-settings-wallet.png" class="img-fluid card-out-img mt-2 mt-lg-4" alt="Settlements">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-5 mt-2 mt-lg-5">
            <div class="card-outline">
                <div class="card-out-body">
                    <p class="card-out-title">Users</p>
                    <div class="row no-gutters">
                        <div class="col-sm-8 col-md-12 col-lg-8">
                                <div class="table-responsive card-out-table d-lg-flex">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td class="simple-text">Invite More Users To Your Organization</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        <div class="col-sm-4 col-md-12 col-lg-4">
                            <div class="img-content">
                                <img src="../img/icon-settings-users.png" class="img-fluid card-out-img mt-2 mt-lg-0" alt="Users">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>