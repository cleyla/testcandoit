<div class="row">
    <div class="col-lg-3">
        <h3>Approved Volume</h3>
    </div>
    <div class="col-lg-9">
        <h3>Tier 3</h3>
    </div>
</div>
<div class="row mt-2 mt-md-4">
    <div class="col-lg-3">
        <div class="content-left">
            <span class="description-cl">
                <p class="bold">Up to $100,000 daily, $5,000,000 annually
                </p>
                <span class="light-description">On average, verification takes 2-4 business days
                    - we'll notify you via email upon approval.</span>
            </span>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-row form-right">
            <div class="col-md-10 mt-3 mt-lg-2">
                <label class="ml-0">The following is required:</label>
                <ul class="custom-list mt-0 mt-md-4">
                    <li>Live website with your products/services available for purchase.</li>
                    <li>A valid government issued Business Tax ID number (United States,
                        Netherlands, Germany).</li>
                    <li>Incorporation/Organization documents or business registration documents.
                    </li>
                    <li>Principal/director/owner(s) photo Identification.
                    </li>
                    <li>Proof of current business address.
                    </li>
                    <li>Three months of business bank statements or a letter of good standing from
                        the bank.</li>
                </ul>
            </div>
            <br></div>
        </div>
    </div>
<div class="row">
    <div class="btn-mg btn-fixed-bottom">
        <a href="#" class="btn-apply ml-auto ml-sm-0">Apply</a>
    </div>
</div>